# updated 3/2005   Andy East  Updated to use perl 5.8.3.809
#
#

use File::Copy;
use Archive::Zip qw(:ERROR_CODES);
use File::Spec;

#zip parameters

my $zippedfile;
my $unzipDir = "L:\\USAT\\FTP\\incoming";


# MAIN
if (@ARGV < 1 || @ARGV > 1)
{
	die <<EOF
	usage: perl unzipfile.pl zipfile.zip 
	Specify full path to zipfile.zip if not in current directory
EOF
}

#retrieve the file name from the command line - should be full path
my $zipName = shift(@ARGV);

unzipfile();

# delete the source file
@filelist = ("$zipName");
unlink @filelist;

exit;

##
## unzip the members of the zip file to the specified directory and preserve the file name
##
sub unzipfile {  
   $zippedfile = $zipName;
 
	$zip = Archive::Zip->new();
	my $status = $zip->read( $zippedfile );
    die "Read of $zippedfile failed\n" if $status != AZ_OK;
 
 	# following two variables are temporary holders not really used.
	my $volume;
	my $directories;
	
	my $dir = $unzipDir;
	
	for my $member ( $zip->members )
	{
		# Don't care about the directory structure when zipped, just the file name itself
		# so must split it up
		($volume,$directories,$thefile) = File::Spec->splitpath( $member->fileName );
		
		# extract the member to the specified working directory and filename
		$member->extractToFileNamed( File::Spec->catfile($dir,$thefile) );
	}   
}