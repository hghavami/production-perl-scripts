#################################################################################
#
# XTRNTDWLD.PL
#
# Created 12/20/2000 by Scott Seller
#
# updated 4/19/2001 Scott Seller
# updated 8/23/2001 Scott Seller - added ability to write to two files so an archive copy is maintained
# updated 8/23/2001 Scott Seller - changed filename to XDMMDDHHSS to allow for multiple files in one day
# XD = "XD", MM = Month, DD = Day, HH = Hour, SS = Seconds
# updated 8/6/2002 Scott Seller - changed logging to create a new file each day.
# updated 3/21/2003 Andy East - Changed logic to write to the archive directory only for the download and report files and 
#                                               then copy those files to the outgoing directory after the processing is complete.
# Updated 09/24/2003 Houman Ghavami - Added field and changed the length of some fields for PTI and SBSTRNAA project
# updated 7/15/2003 Andy East - Changed logic to report on Complaint entries 
# Updated 05/17/2004 Andy East - Add logic to encrypt data and FTP to public Enterprise FTP Server
# updated 3/2005   Andy East  Updated to use perl 5.8.3.809
#							  Updated for GMTI directory structure
# updated 7/2011   Houman Ghavami	Replaced FTP with sFTP.
#
# Daily processing of updates to the AS/400
#
# Reads all rows in XTRNTDWLD.
# Creates XDMMDDHHSS and FTPs it to the EFTP system

# This script is triggered by a scheduled task.
#
#
#################################################################################

use Win32::ODBC;
use Net::SMTP;
use File::Copy;
use File::stat;
use File::Util;
use Net::FTP;
use Win32::Process;
use Date::Format;
use chilkat;


# Configure following based on prod/test environment
my $debug = "ON";
my $login = "extftptest";   			# ftp login
my $slogin = "usat-webportaltest";		# sftp login
my $pass = "19extftptest99";    		# ftp password
my $spass = "pBIsm9vS5JSiZsGFpecr";		# sftp password
my $server = "159.54.128.31";  			# ftp server
my $sserver = "159.54.32.85";			# sftp server
my $sport = "22";						# sftp port

my $todir = "incoming";  				# destination server directory to put file
my $stodir = "/incoming/";

my $rootDir = "C:\\USAT\\";
my $workingDir = $rootDir . "processed\\D4\\archive\\";
my $homedir = "c:\\gnupg";  #location of the GNU keystore

$mailServer = "10.209.4.25";

my $encryptedfile = "";  #holder vaiable for name of encrypted file
my $filetoencrypt = "";  #holder variable for source file to encrypt
my $fileToSend = "";     #holder variable for the current file to ftp

my($db) = new Win32::ODBC("dsn=esub_sp_staging_d4; uid=esub; pwd=ueat12");

$db->SetConnectOption('SQL_AUTOCOMMIT', 'SQL_AUOTCOMMIT_OFF');

# Create archive file name based on current date.
$current_time = localtime;
&CreateTimeStamp();

$archive_filename= "XTRNTDWLD_" . "$month$day$hour$sec";
$report_archive_filename = "XTRNTDWLD_REPORT_" . "$year$month$day$hour$min\.txt";
$extract_file_name = "$workingDir" . "XD" . "$month$day$hour$sec" . ".gpg";
my $localextract_file_name = "XD" . "$month$day$hour$sec" ;
## Delete following one line after transission period - APE 5/20/2004
#$old_extract_file_name = "XD" . "$month$day$hour$sec";

$LogFileTime = $year . $month . $day;
$LogFileName = "XTRNTDWLD_" . $LogFileTime . ".log";

open (LOG_FILE, ">>" . $rootDir . "processed\\D4\\logs\\$LogFileName") || die "cannot open the file";
open (ARCHIVE_EXTRACT_FILE, ">$workingDir$archive_filename") || die "cannot open the file";
open (ARCHIVE_REPORT_FILE, ">$workingDir$report_archive_filename") || die "cannot open the file";

print LOG_FILE "\n\nStarted creating $workingDir$archive_filename at $current_time\n";

### Create email/page list

if (open(NL, "C:\\USAT\\scripts\\D4\\notify_list.txt"))
	{
		@notify = (<NL>);
		close NL;
	}
else
	{
		print "Error opening notify_list.txt\n";
		print LOG_FILE "$timestamp -- Error opening notify_list.txt\n";
		die;
}

# set records to in process	
$db->Sql("Update XTRNTDWLD SET TransactionState = 1 where TransactionState = 0");
$db->Transact(SQL_COMMIT);


$db->Sql("SELECT * FROM XTRNTDWLD where TransactionState = 1");

while ($db->FetchRow())
	{
		my(%data) = $db->DataHash();

		$PubCode = sprintf("%-2.2s",$data{'PubCode'});  
		$TranDate = sprintf("%8.8s",$data{'TranDate'}); 
		$TranSeqNum = sprintf("%5.5s",$data{'TranSeqNum'}); 
		$TranRecTyp = sprintf("%-2.2s",$data{'TranRecTyp'});
		$AddrTyp = sprintf("%-1.1s",$data{'AddrTyp'});  
		$LastName = sprintf("%-15.15s",$data{'LastName'}); 
		$FirstName = sprintf("%-10.10s",$data{'FirstName'});
		$FirmName = sprintf("%-28.28s",$data{'FirmName'}); 
		$AddAddr1 = sprintf("%-28.28s",$data{'AddAddr1'}); 
		$AddAddr2 = sprintf("%-28.28s",$data{'AddAddr2'}); 
		$UnitNum = sprintf("%-10.10s",$data{'UnitNum'}); 
		$HalfUnitNum = sprintf("%-3.3s",$data{'HalfUnitNum'});
		$StreetDir = sprintf("%-2.2s",$data{'StreetDir'});
		$StreetName = sprintf("%-28.28s",$data{'StreetName'});
		$StreetTyp = sprintf("%-4.4s",$data{'StreetTyp'});
		$StreetPstDir = sprintf("%-2.2s",$data{'StreetPstDir'});
		$SubUnitCode = sprintf("%-4.4s",$data{'SubUnitCode'});
		$SubUnitNum = sprintf("%-8.8s",$data{'SubUnitNum'}); 
		$City = sprintf("%-28.28s",$data{'City'});  
		$ShortCity = sprintf("%-13.13s",$data{'ShortCity'});
		$State = sprintf("%-2.2s",$data{'State'}); 
		$Zip5 = sprintf("%-5.5s",$data{'Zip5'});  
		$Zip4 = sprintf("%-4.4s",$data{'Zip4'});  
		$DelPtBarCode = sprintf("%-2.2s",$data{'DelPtBarCode'});
		$CarrRteSort = sprintf("%-4.4s",$data{'CarrRteSort'});
		if ($data{'HomePhone'} eq "0") {$HomePhone = sprintf("%10.10s",'');} else {$HomePhone = sprintf("%10.10s",$data{'HomePhone'});}
		if ($data{'BusPhone'} eq 0) {$BusPhone = sprintf("%10.10s",'');} else {$BusPhone = sprintf("%10.10s",$data{'BusPhone'});}
		$GatedCommFlg = sprintf("%-1.1s",$data{'GatedCommFlg'});
		$GateSecCode = sprintf("%-5.5s",$data{'GateSecCode'});
		$Code1ErrCode = sprintf("%-4.4s",$data{'Code1ErrCode'});
		$PIATranCode = sprintf("%-4.4s",$data{'PIATranCode'});
		$BatchNum = sprintf("%-4.4s",$data{'BatchNum'}); 
		$CurrPrtSiteCode = sprintf("%-2.2s",$data{'CurrPrtSiteCode'});
		$CurrMktcode = sprintf("%-4.4s",$data{'CurrMktcode'});
		if ($data{'CurrAcctNum'} eq 0) {$CurrAcctNum = sprintf("%7.7s",'');} else {$CurrAcctNum = sprintf("%7.7s",substr($data{'CurrAcctNum'},2,7));}          
		if ($data{'CurrAddrNum'} eq 0) {$CurrAddrNum = sprintf("%7.7s",'');} else {$CurrAddrNum = sprintf("%7.7s",$data{'CurrAddrNum'});}
		if ($data{'Zone'} eq 0) {$Zone = sprintf("%2.2s",'');} else {$Zone = sprintf("%2.2s",$data{'Zone'});}
		if ($data{'District'} eq 0) {$District = sprintf("%2.2s",'');} else {$District = sprintf("%2.2s",$data{'District'});}
		if ($data{'Route'} eq 0) {$Route = sprintf("%3.3s",'');} else {$Route = sprintf("%3.3s",$data{'Route'});}
		$TranType = sprintf("%-1.1s",$data{'TranType'}); 
		$TranCode = sprintf("%-2.2s",$data{'TranCode'}); 
		if ($data{'RadPostdDt'} eq 0) {$RadPostdDt = sprintf("%7.7s",'');} else {$RadPostdDt = sprintf("%7.7s",$data{'RadPostdDt'});}
		if ($data{'RadPostdTm'} eq 0) {$RadPostdTm = sprintf("%6.6s",'');} else {$RadPostdTm = sprintf("%6.6s",$data{'RadPostdTm'});}
		$OperID = sprintf("%-2.2s",$data{'OperID'});
		$MktSeg = sprintf("%-2.2s",$data{'MktSeg'});
		$PrizmCode = sprintf("%-2.2s",$data{'PrizmCode'});
		$PrizmGrp = sprintf("%-2.2s",$data{'PrizmGrp'}); 
		$FIPSState = sprintf("%-2.2s",$data{'FIPSState'});
		$FIPSCounty = sprintf("%-3.3s",$data{'FIPSCounty'});
		$CensusTract = sprintf("%-6.6s",$data{'CensusTract'});
		$CensusBlkGrp = sprintf("%-1.1s",$data{'CensusBlkGrp'});
		$Latitude = sprintf("%-8.8s",$data{'Latitude'}); 
		$Longtitude = sprintf("%-8.8s",$data{'Longtitude'});
		$VicsSrcOrdCode = sprintf("%-1.1s",$data{'VicsSrcOrdCode'});
		$VicsSubsType = sprintf("%-1.1s",$data{'VicsSubsType'});
		$VicsFreqDel = sprintf("%-2.2s",$data{'VicsFreqDel'});
		if ($data{'VicsNumPaper'} eq 0) {$VicsNumPaper = sprintf("%3.3s",'');} else {$VicsNumPaper = sprintf("%3.3s",$data{'VicsNumPaper'});}
		$VicsRateCode = sprintf("%-2.2s",$data{'VicsRateCode'});
		if ($data{'VicsSubsLngth'} eq 0) {$VicsSubsLngth = sprintf("%3.3s",'');} else {$VicsSubsLngth = sprintf("%3.3s",$data{'VicsSubsLngth'});}
		$VicsSoldBy = sprintf("%-11.11s",$data{'VicsSoldBy'});
		$VicsPromoCode = sprintf("%-2.2s",$data{'VicsPromoCode'}); 
		$VicsContestCode = sprintf("%-2.2s",$data{'VicsContestCode'});
		if ($data{'VicsRebilCode'} eq 0) {$VicsRebilCode = sprintf("%1.1s",'');} else {$VicsRebilCode = sprintf("%1.1s",$data{'VicsRebilCode'});}
		$VicsSpecialRunCode = sprintf("%-2.2s",$data{'VicsSpecialRunCode'});
		$FreqDel = sprintf("%-2.2s",$data{'FreqDel'});  
		$ExstDelMethod = sprintf("%-1.1s",$data{'ExstDelMethod'}); 
		$RateCode = sprintf("%-2.2s",$data{'RateCode'}); 
		$PromoCode = sprintf("%-2.2s",$data{'PromoCode'});
		if ($data{'NumPaper'} eq 0) {$NumPaper = sprintf("%3.3s",'');} else {$NumPaper = sprintf("%3.3s",$data{'NumPaper'});}
		$ContestCode = sprintf("%-2.2s",$data{'ContestCode'});
		$SrcOrdCode = sprintf("%-1.1s",$data{'SrcOrdCode'});
		$SpecialRunCode = sprintf("%-2.2s",$data{'SpecialRunCode'});
		if ($data{'RebillCode'} eq 0) {$RebillCode = sprintf("%1.1s",'');} else {$RebillCode = sprintf("%1.1s",$data{'RebillCode'});}
		$SubsType = sprintf("%-1.1s",$data{'SubsType'}); 
		$SubsAmount = sprintf("%7.7s",$data{'SubsAmount'});
		if ($data{'SubsAmount'} eq "0.00") {$SubsAmount = sprintf("%7.7s",'');} else {$SubsAmount = sprintf("%7.7s",$data{'SubsAmount'});}
		if ($data{'SubsDur'} eq 0) {$SubsDur = sprintf("%3.3s",'');} else {$SubsDur = sprintf("%3.3s",$data{'SubsDur'});}
		$Premium = sprintf("%-2.2s",$data{'Premium'});  
		if ($data{'PayAmount'} eq "0.00") {$PayAmount = sprintf("%7.7s",'');} else {$PayAmount = sprintf("%7.7s",$data{'PayAmount'});}
		$IssuePdUnpd = sprintf("%-1.1s",$data{'IssuePdUnpd'});
		$TaxExempt = sprintf("%-20.20s",$data{'TaxExempt'});
		$SoldBy = sprintf("%-11.11s",$data{'SoldBy'});
		$MarketCode = sprintf("%-3.3s",$data{'MarketCode'});
		$Status = sprintf("%-1.1s",$data{'Status'});
		$EntryMethod = sprintf("%-1.1s",$data{'EntryMethod'});
		$AgencyName = sprintf("%-4.4s",$data{'AgencyName'});
		$PrintFlag = sprintf("%-1.1s",$data{'PrintFlag'});
		$PndLookSts = sprintf("%-1.1s",$data{'PndLookSts'});
		$VIPLabel = sprintf("%-1.1s",$data{'VIPLabel'}); 
		$TtlMktCov = sprintf("%-1.1s",$data{'TtlMktCov'});
		$DeptRef = sprintf("%-3.3s",$data{'DeptRef'});  
		$ABCCode = sprintf("%-3.3s",$data{'ABCCode'});  
		$ClubNum = sprintf("%-15.15s",$data{'ClubNum'});  
		$ClubType = sprintf("%-1.1s",$data{'ClubType'}); 
		$PurOrdNum = sprintf("%-35.35s",$data{'PurOrdNum'});
		$SalesRep = sprintf("%-4.4s",$data{'SalesRep'}); 
		$OvrCode1 = sprintf("%-1.1s",$data{'OvrCode1'}); 
		$DltGiftPayer = sprintf("%-1.1s",$data{'DltGiftPayer'});
		$PerpCCFlag = sprintf("%-1.1s",$data{'PerpCCFlag'});
		$BillChargePd = sprintf("%-1.1s",$data{'BillChargePd'});
		$OneTimeBill = sprintf("%-1.1s",$data{'OneTimeBill'});
		$CCType = sprintf("%-1.1s",$data{'CCType'});
		$CCNum = sprintf("%-200.200s",$data{'CCNum'});
		$CCExpireDt = sprintf("%-4.4s",$data{'CCExpireDt'});
		$NewAddrType = sprintf("%-1.1s",$data{'NewAddrType'});
		$NewLastName = sprintf("%-15.15s",$data{'NewLastName'});
		$NewFirstName = sprintf("%-10.10s",$data{'NewFirstName'});
		$NewFirmName = sprintf("%-28.28s",$data{'NewFirmName'});
		$NewAddlAddr1 = sprintf("%-28.28s",$data{'NewAddlAddr1'});
		$NewAddlAddr2 = sprintf("%-28.28s",$data{'NewAddlAddr2'});
		$NewUnitNum = sprintf("%-10.10s",$data{'NewUnitNum'});
		$NewHalfUnitNum = sprintf("%-3.3s",$data{'NewHalfUnitNum'});
		$NewStreetDir = sprintf("%-2.2s",$data{'NewStreetDir'});
		$NewStreetName = sprintf("%-28.28s",$data{'NewStreetName'}); 
		$NewStreetType = sprintf("%-4.4s",$data{'NewStreetType'}); 
		$NewStreetPostDir = sprintf("%-2.2s",$data{'NewStreetPostDir'});  
		$NewSubUnitNum = sprintf("%-8.8s",$data{'NewSubUnitNum'}); 
		$NewSubUnitCode = sprintf("%-4.4s",$data{'NewSubUnitCode'});
		$NewCity = sprintf("%-28.28s",$data{'NewCity'});  
		$NewShortCity = sprintf("%-13.13s",$data{'NewShortCity'});
		$NewState = sprintf("%-2.2s",$data{'NewState'}); 
		$NewZip5 = sprintf("%-5.5s",$data{'NewZip5'});  
		$NewZip4 = sprintf("%-4.4s",$data{'NewZip4'});  
		$NewDelPtBarCode = sprintf("%-2.2s",$data{'NewDelPtBarCode'});
		$NewCarrRteSort = sprintf("%-4.4s",$data{'NewCarrRteSort'});
		$NewGatedCommFlag = sprintf("%-1.1s",$data{'NewGatedCommFlag'});  
		$NewGatedCommSecCode = sprintf("%-5.5s",$data{'NewGatedCommSecCode'});  
		if ($data{'NewPhoneNum'} eq "0") {$NewPhoneNum = sprintf("%10.10s",'');} else {$NewPhoneNum = sprintf("%10.10s",$data{'NewPhoneNum'});}
		if ($data{'NewBusPhoneNum'} eq "0") {$NewBusPhoneNum = sprintf("%10.10s",'');} else {$NewBusPhoneNum = sprintf("%10.10s",$data{'NewBusPhoneNum'});}
		if ($data{'NewAddrNum'} eq "0") {$NewAddrNum = sprintf("%7.7s",'');} else {$NewAddrNum = sprintf("%7.7s",$data{'NewAddrNum'});}
		$TRFSecRecType = sprintf("%-2.2s",$data{'TRFSecRecType'}); 
		if ($data{'InfoRTY2Based'} eq 0) {$InfoRTY2Based = sprintf("%7.7s",'');} else {$InfoRTY2Based = sprintf("%7.7s",$data{'InfoRTY2Based'});}
		if ($data{'NewAcctNum'} eq "0") {$NewAcctNum = sprintf("%7.7s",'');} else {$NewAcctNum = sprintf("%7.7s",$data{'NewAcctNum'});}
		$TranferType = sprintf("%-1.1s",$data{'TranferType'});
		$NewPrintsiteCode = sprintf("%-2.2s",$data{'NewPrintsiteCode'});  
		$NewMktcode = sprintf("%-4.4s",$data{'NewMktcode'});
		$NewSiteCode = sprintf("%-3.3s",$data{'NewSiteCode'});
		$NewDelMethod = sprintf("%-1.1s",$data{'NewDelMethod'});
		$NewMktSeg = sprintf("%-2.2s",$data{'NewMktSeg'});
		$NewPrizmCode = sprintf("%-2.2s",$data{'NewPrizmCode'});
		$NewPrizmGrp = sprintf("%-2.2s",$data{'NewPrizmGrp'});
		$NewFIPSState = sprintf("%-2.2s",$data{'NewFIPSState'});
		$NewFIPSCounty = sprintf("%-3.3s",$data{'NewFIPSCounty'}); 
		$NewCensusTrack = sprintf("%-6.6s",$data{'NewCensusTrack'});
		$NewCensusBlkGrp = sprintf("%-1.1s",$data{'NewCensusBlkGrp'});
		$NewLatitude = sprintf("%-8.8s",$data{'NewLatitude'});
		$NewLongitude = sprintf("%-8.8s",$data{'NewLongitude'});
		$ContactCust = sprintf("%-1.1s",$data{'ContactCust'});
		if ($data{'CreditDay1'} eq 0) {$CreditDay1 = sprintf("%2.2s",'');} else {$CreditDay1 = sprintf("%2.2s",$data{'CreditDay1'});}
		if ($data{'CreditDay2'}eq 0) {$CreditDay2 = sprintf("%2.2s",'');} else {$CreditDay2 = sprintf("%2.2s",$data{'CreditDay2'});}
		if ($data{'CreditDay3'} eq 0) {$CreditDay3 = sprintf("%2.2s",'');} else {$CreditDay3 = sprintf("%2.2s",$data{'CreditDay3'});}
		if ($data{'CreditDay4'} eq 0) {$CreditDay4 = sprintf("%2.2s",'');} else {$CreditDay4 = sprintf("%2.2s",$data{'CreditDay4'});}
		if ($data{'CreditDay5'} eq 0) {$CreditDay5 = sprintf("%2.2s",'');} else {$CreditDay5 = sprintf("%2.2s",$data{'CreditDay5'});}
		if ($data{'CreditDay6'} eq 0) {$CreditDay6 = sprintf("%2.2s",'');} else {$CreditDay6 = sprintf("%2.2s",$data{'CreditDay6'});}
		if ($data{'CreditDay7'} eq 0) {$CreditDay7 = sprintf("%2.2s",'');} else {$CreditDay7 = sprintf("%2.2s",$data{'CreditDay7'});}
		if ($data{'DebtAdjAmount'} eq "0.00000") {$DebtAdjAmount = sprintf("%11.11s",'');} else {$DebtAdjAmount = sprintf("%11.11s",$data{'DebtAdjAmount'});}
		if ($data{'CreditAdjAmount'} eq "0.00000") {$CreditAdjAmount = sprintf("%11.11s",'');} else {$CreditAdjAmount = sprintf("%11.11s",$data{'CreditAdjAmount'});}
		$DelPref = sprintf("%-1.1s",$data{'DelPref'});  
		$Comment = sprintf("%-38.38s",$data{'Comment'});  
		$RejComm = sprintf("%-50.50s",$data{'RejComm'});  
		$AutoRegComm = sprintf("%-50.50s",$data{'AutoRegComm'});
		if ($data{'EffDate1'} eq 0) {$EffDate1 = sprintf("%8.8s",'');} else {$EffDate1 = sprintf("%8.8s",$data{'EffDate1'});}
		if ($data{'EffDate2'} eq 0) {$EffDate2 = sprintf("%8.8s",'');} else {$EffDate2 = sprintf("%8.8s",$data{'EffDate2'});}
		$BillAddrType = sprintf("%-1.1s",$data{'BillAddrType'});
		$BillLastName = sprintf("%-15.15s",$data{'BillLastName'});
		$BillFirstName = sprintf("%-10.10s",$data{'BillFirstName'}); 
		$BillFirmName = sprintf("%-28.28s",$data{'BillFirmName'});
		$BillAddr1 = sprintf("%-28.28s",$data{'BillAddr1'});
		$BillAddr2 = sprintf("%-28.28s",$data{'BillAddr2'});
		$BillUnitNum = sprintf("%-10.10s",$data{'BillUnitNum'});
		$BillHalfUnitNum = sprintf("%-3.3s",$data{'BillHalfUnitNum'});
		$BillStreetDir = sprintf("%-2.2s",$data{'BillStreetDir'}); 
		$BillStreetName = sprintf("%-28.28s",$data{'BillStreetName'});
		$BillStreetType = sprintf("%-4.4s",$data{'BillStreetType'});
		$BillStreetPostDir = sprintf("%-2.2s",$data{'BillStreetPostDir'}); 
		$BillSubUnitNum = sprintf("%-8.8s",$data{'BillSubUnitNum'});
		$BillSubUnitCode = sprintf("%-4.4s",$data{'BillSubUnitCode'});
		$BillCity = sprintf("%-28.28s",$data{'BillCity'}); 
		$BillShortCity = sprintf("%-13.13s",$data{'BillShortCity'}); 
		$BillState = sprintf("%-2.2s",$data{'BillState'});
		$BillZip5 = sprintf("%-5.5s",$data{'BillZip5'}); 
		$BillZip4 = sprintf("%-4.4s",$data{'BillZip4'}); 
		$BillDelPtBarCode = sprintf("%-2.2s",$data{'BillDelPtBarCode'});  
		$BillCarrRteSort = sprintf("%-4.4s",$data{'BillCarrRteSort'});
		if ($data{'BillBusPhone'} eq 0) {$BillBusPhone = sprintf("%10.10s",'');} else {$BillBusPhone = sprintf("%10.10s",$data{'BillBusPhone'});}
		if ($data{'BillHomePhone'} eq 0) {$BillHomePhone = sprintf("%10.10s",'');} else {$BillHomePhone = sprintf("%10.10s",$data{'BillHomePhone'});}
		$CCAuthCode = sprintf("%-6.6s",$data{'CCAuthCode'});
		$CCAuthDate = sprintf("%-8.8s",$data{'CCAuthDate'});
		$CCBatchProc = sprintf("%-1.1s",$data{'CCBatchProc'});
		$CCAuthRejDesc = sprintf("%-75.75s",$data{'CCAuthRejDesc'});
		$CCCIDCode = sprintf("%-4.4s",$data{'CCCIDCode'});
		$AppFlag = sprintf("%-1.1s",$data{'AppFlag'});
		$BankRtNum = sprintf("%-9.9s",$data{'BankRtNum'});
		$BankActNum = sprintf("%-13.13s",$data{'BankActNum'});
		$CheckNum = sprintf("%-12.12s",$data{'CheckNum'});
		$MicrTranCode = sprintf("%-6.6s",$data{'MicrTranCode'});
		$WebID = sprintf("%-20.20s",$data{'WebID'});
		$DiscAgncyFlg = sprintf("%-1.1s",$data{'DiscAgncyFlg'});
		$EmailType = sprintf("%-1.1s",$data{'EmailType'});
		$EmailAddress = sprintf("%-60.60s",$data{'EmailAddress'});
		$GiftCard = sprintf("%-1.1s",$data{'GiftCard'});
		$BadSubAddress = sprintf("%-56.56s",$data{'BadSubAddress'});
		$BadTrnGftAddress = sprintf("%-56.56s",$data{'BadTrnGftAddress'});
		$FulFillItem = sprintf("%-1.1s",$data{'FulFillItem'});
		$GiftPayerTMC = sprintf("%-1.1s",$data{'GiftPayerTMC'});
		$PremType = sprintf("%-2.2s",$data{'PremType'});
		$PremAttr = sprintf("%-2.2s",$data{'PremAttr'});
		$Filler = sprintf("%-474.474s",$data{'Filler'});
		
		# Calculate number of rows being processed as well as transaction types
		if ($PubCode eq "UT") 
			{
			PubCodeUT: 
				{
				if ($TranRecTyp eq "01") {$UT01++; last PubCodeUT;}
				if ($TranRecTyp eq "02") {$UT02++; last PubCodeUT;}
				if ($TranRecTyp eq "03") {$UT03++; last PubCodeUT;}
				if ($TranRecTyp eq "04") {$UT04++; last PubCodeUT;}
				if ($TranRecTyp eq "06") {$UT06++; last PubCodeUT;}
				if ($TranRecTyp eq "07") {$UT07++; last PubCodeUT;}
				if ($TranRecTyp eq "11") {$UT11++; last PubCodeUT;}
				$OtherUT++;
				}
			}
		elsif ($PubCode eq "BW") 
			{
			PubCodeBW: 
				{
				if ($TranRecTyp eq "01") {$BW01++; last PubCodeBW;}
				if ($TranRecTyp eq "02") {$BW02++; last PubCodeBW;}
				if ($TranRecTyp eq "03") {$BW03++; last PubCodeBW;}
				if ($TranRecTyp eq "04") {$BW04++; last PubCodeBW;}
				if ($TranRecTyp eq "06") {$BW06++; last PubCodeBW;}
				if ($TranRecTyp eq "07") {$BW07++; last PubCodeBW;}
				if ($TranRecTyp eq "11") {$BW11++; last PubCodeBW;}
				$OtherBW++;
				}
			}
		elsif ($PubCode eq "EE") 
			{
			PubCodeEE: 
				{
				if ($TranRecTyp eq "01") {$EE01++; last PubCodeEE;}
				if ($TranRecTyp eq "02") {$EE02++; last PubCodeEE;}
				if ($TranRecTyp eq "03") {$EE03++; last PubCodeEE;}
				if ($TranRecTyp eq "04") {$EE04++; last PubCodeEE;}
				if ($TranRecTyp eq "06") {$EE06++; last PubCodeEE;}
				if ($TranRecTyp eq "07") {$EE07++; last PubCodeEE;}
				if ($TranRecTyp eq "11") {$EE11++; last PubCodeEE;}
				$OtherEE++;
				}
			}
		else {
			PubCodeOTHER: 
				{
				if ($TranRecTyp eq "01") {$OTHER01++; last PubCodeOTHER;}
				if ($TranRecTyp eq "02") {$OTHER02++; last PubCodeOTHER;}
				if ($TranRecTyp eq "03") {$OTHER03++; last PubCodeOTHER;}
				if ($TranRecTyp eq "04") {$OTHER04++; last PubCodeOTHER;}
				if ($TranRecTyp eq "06") {$OTHER06++; last PubCodeOTHER;}
				if ($TranRecTyp eq "07") {$OTHER07++; last PubCodeOTHER;}
				if ($TranRecTyp eq "11") {$OTHER11 = $OTHER11++; last PubCodeOTHER;}
				$OtherPub++;
				}
			}

		$TextString = "$PubCode" . "$TranDate" . "$TranSeqNum" . "$TranRecTyp" . "$AddrTyp" . "$LastName" . "$FirstName" . 
					"$FirmName" . "$AddAddr1" . "$AddAddr2" . "$UnitNum" . "$HalfUnitNum" . "$StreetDir" . "$StreetName" . 
					"$StreetTyp" . "$StreetPstDir" . "$SubUnitCode" . "$SubUnitNum" . "$City" . "$ShortCity" . "$State"  .	
					"$Zip5" . "$Zip4" . "$DelPtBarCode" . "$CarrRteSort" . "$HomePhone" . "$BusPhone" . "$GatedCommFlg" . 
					"$GateSecCode" . "$Code1ErrCode" . "$PIATranCode" . "$BatchNum" . "$CurrPrtSiteCode" . "$CurrMktcode" . 
					"$CurrAcctNum" . "$CurrAddrNum" . "$Zone" . "$District" . "$Route" . "$TranType" . "$TranCode" . "$RadPostdDt" .
					"$RadPostdTm" . "$OperID" . "$MktSeg" . "$PrizmCode" . "$PrizmGrp" . "$FIPSState" . "$FIPSCounty" . "$CensusTract" . 
					"$CensusBlkGrp" . "$Latitude" . "$Longtitude" . "$VicsSrcOrdCode" . "$VicsSubsType" . "$VicsFreqDel" . 
					"$VicsNumPaper" . "$VicsRateCode" . "$VicsSubsLngth" . "$VicsSoldBy" . "$VicsPromoCode" . "$VicsContestCode" . 
					"$VicsRebilCode" . "$VicsSpecialRunCode" . "$FreqDel" . "$ExstDelMethod" . "$RateCode" . "$PromoCode" . 
					"$NumPaper" . "$ContestCode" . "$SrcOrdCode" . "$SpecialRunCode" . "$RebillCode" . "$SubsType" . "$SubsAmount" . 
					"$SubsDur" . "$Premium" . "$PayAmount" . "$IssuePdUnpd" . "$TaxExempt" . "$SoldBy" . "$MarketCode" . "$Status" . 
					"$EntryMethod" . "$AgencyName" . "$PrintFlag" . "$PndLookSts" . "$VIPLabel" . "$TtlMktCov" . "$DeptRef" . 
					"$ABCCode" . "$ClubNum" . "$ClubType" . "$PurOrdNum" . "$SalesRep" . "$OvrCode1" . "$DltGiftPayer" . 
					"$PerpCCFlag" . "$BillChargePd" . "$OneTimeBill" . "$CCType" . "$CCNum" . "$CCExpireDt" . "$NewAddrType" . 
					"$NewLastName" . "$NewFirstName" . "$NewFirmName" . "$NewAddlAddr1" . "$NewAddlAddr2" . "$NewUnitNum" . 
					"$NewHalfUnitNum" . "$NewStreetDir" . "$NewStreetName" . "$NewStreetType" . "$NewStreetPostDir" . 
					"$NewSubUnitNum" . "$NewSubUnitCode" . "$NewCity" . "$NewShortCity" . "$NewState" . "$NewZip5" . "$NewZip4" . 
					"$NewDelPtBarCode" . "$NewCarrRteSort" . "$NewGatedCommFlag" . "$NewGatedCommSecCode" . "$NewPhoneNum" . 
					"$NewBusPhoneNum" . "$NewAddrNum" . "$TRFSecRecType" . "$InfoRTY2Based" . "$NewAcctNum" . "$TranferType" . 
					"$NewPrintsiteCode" . "$NewMktcode" . "$NewSiteCode" . "$NewDelMethod" . "$NewMktSeg" . "$NewPrizmCode" . 
					"$NewPrizmGrp" . "$NewFIPSState" . "$NewFIPSCounty" . "$NewCensusTrack" . "$NewCensusBlkGrp" . "$NewLatitude" . 
					"$NewLongitude" . "$ContactCust" . "$CreditDay1" . "$CreditDay2" . "$CreditDay3" . "$CreditDay4" . 
					"$CreditDay5" . "$CreditDay6" . "$CreditDay7" . "$DebtAdjAmount" . "$CreditAdjAmount" . "$DelPref" . 
					"$Comment" . "$RejComm" . "$AutoRegComm" . "$EffDate1" . "$EffDate2" . "$BillAddrType" . "$BillLastName" . 
					"$BillFirstName" . "$BillFirmName" . "$BillAddr1" . "$BillAddr2" . "$BillUnitNum" . "$BillHalfUnitNum" . 
					"$BillStreetDir" . "$BillStreetName" . "$BillStreetType" . "$BillStreetPostDir" . "$BillSubUnitNum" . 
					"$BillSubUnitCode" . "$BillCity" . "$BillShortCity" . "$BillState" . "$BillZip5" . "$BillZip4" . 
					"$BillDelPtBarCode" . "$BillCarrRteSort" . "$BillBusPhone" . "$BillHomePhone" . 
					"$CCAuthCode" . "$CCAuthDate" . "$CCBatchProc" . "$CCAuthRejDesc" . "$CCCIDCode" . "$AppFlag" . "$BankRtNum" . 
					"$BankActNum" . "$CheckNum" . "$MicrTranCode" . "$WebID" . "$DiscAgncyFlg" . "$EmailType" . "$EmailAddress" . 
					"$GiftCard" . "$BadSubAddress" . "$BadTrnGftAddress" . "$FulFillItem" . "$GiftPayerTMC" . "$PremType" . 
					"$PremAttr" . "$Filler" . "\n";
		
		for $extract_filehandle ('ARCHIVE_EXTRACT_FILE')
			{
				print $extract_filehandle "$TextString";
			}

		$TotalRecords++

} # while ($db->FetchRow())

$db->Sql("DELETE FROM XTRNTDWLD where TransactionState = 1");
$db->Transact(SQL_COMMIT);

$db->Close();

# Print record type report to file

$runtime = localtime;
for $report_filehandle ('ARCHIVE_REPORT_FILE', 'LOG_FILE')
	{
		print $report_filehandle "Record types being transferred to the AS/400 (XTRNTDWLD)\n";
		print $report_filehandle "\n";
		print $report_filehandle "$runtime\n";
		print $report_filehandle "\n";
		print $report_filehandle "PubCode UT:\n";
		print $report_filehandle "\tTransaction Type 01: \t\t$UT01\n";
		print $report_filehandle "\tTransaction Type 02: \t\t$UT02\n";
		print $report_filehandle "\tTransaction Type 03: \t\t$UT03\n";
		print $report_filehandle "\tTransaction Type 04: \t\t$UT04\n";
		print $report_filehandle "\tTransaction Type 06: \t\t$UT06\n";
		print $report_filehandle "\tTransaction Type 07: \t\t$UT07\n";
		print $report_filehandle "\tTransaction Type 11: \t\t$UT11\n";
		print $report_filehandle "\tOther Transaction Types: \t$OtherUT\n";
		print $report_filehandle "\n";
		
		print $report_filehandle "PubCode BW:\n";
		print $report_filehandle "\tTransaction Type 01: \t\t$BW01\n";
		print $report_filehandle "\tTransaction Type 02: \t\t$BW02\n";
		print $report_filehandle "\tTransaction Type 03: \t\t$BW03\n";
		print $report_filehandle "\tTransaction Type 04: \t\t$BW04\n";
		print $report_filehandle "\tTransaction Type 06: \t\t$BW06\n";
		print $report_filehandle "\tTransaction Type 07: \t\t$BW07\n";
		print $report_filehandle "\tTransaction Type 11: \t\t$BW11\n";
		print $report_filehandle "\tOther Transaction Types: \t$OtherBW\n";
		print $report_filehandle "\n";

		print $report_filehandle "PubCode EE:\n";
		print $report_filehandle "\tTransaction Type 01: \t\t$EE01\n";
		print $report_filehandle "\tTransaction Type 02: \t\t$EE02\n";
		print $report_filehandle "\tTransaction Type 03: \t\t$EE03\n";
		print $report_filehandle "\tTransaction Type 04: \t\t$EE04\n";
		print $report_filehandle "\tTransaction Type 06: \t\t$EE06\n";
		print $report_filehandle "\tTransaction Type 07: \t\t$EE07\n";
		print $report_filehandle "\tTransaction Type 11: \t\t$EE11\n";
		print $report_filehandle "\tOther Transaction Types: \t$OtherEE\n";
		print $report_filehandle "\n";

		print $report_filehandle "PubCode OTHER:\n";
		print $report_filehandle "\tTransaction Type 01: \t\t$OTHER01\n";
		print $report_filehandle "\tTransaction Type 02: \t\t$OTHER02\n";
		print $report_filehandle "\tTransaction Type 03: \t\t$OTHER03\n";
		print $report_filehandle "\tTransaction Type 04: \t\t$OTHER04\n";
		print $report_filehandle "\tTransaction Type 06: \t\t$OTHER06\n";
		print $report_filehandle "\tTransaction Type 07: \t\t$OTHER07\n";
		print $report_filehandle "\tTransaction Type 11: \t\t$OTHER11\n";
		print $report_filehandle "\tOther Transaction Types: \t$OtherPub\n";
		print $report_filehandle "\n";

		print $report_filehandle "Total Records Processed: \t\t$TotalRecords\n";

	}

$end_time = localtime;
print LOG_FILE "Finished creating source data file at $end_time\n";

close (ARCHIVE_EXTRACT_FILE);
close (ARCHIVE_REPORT_FILE);

# encrypt transaction file
$encryptedfile = $extract_file_name;
$filetoencrypt = $workingDir . $archive_filename;
encryptStuff();

$end_time = localtime;

# if encrypted file exists
if (-e $encryptedfile)
{ 
	print LOG_FILE "Finished encrypting $extract_file_name at $end_time\n";
}
else {
	print LOG_FILE "Failed to encrypt $extract_file_name at $end_time\n";
	&send_alert("$end_time -- FAILED To Encrypt Download file for file $extract_file_name\n\n  MANUAL SEND REQUIRED !", "GNUPGP Encrypt Failed (transaction file)");
	print LOG_FILE "Alert Sent. Program Terminating.\n\n";
	die;
}


# ftp transaction file
$fileToSend = $encryptedfile;
#sendftp();
send_sFTP();
$end_time = localtime;
#copy("$workingDir$archive_filename",$rootDir . "FTP\\d4\\incoming\\$localextract_file_name") || die "cannot copy the extract file";
print LOG_FILE "Finished Copying $localextract_file_name at $end_time\n";

#copy the report file to the outgoing directory for pickup by the FileScan tool
#copy("$workingDir$report_archive_filename",$rootDir . "FTP\\d4\\outgoing\\$report_archive_filename") || die "cannot copy the report file";
copy("$workingDir$report_archive_filename","C:\\USAT\\FTP\\D4\\outgoing\\$report_archive_filename") || die "cannot copy the report file";

# UNCOMMNET FOLLOWING LINES FOLLOWING INTERIM PERIOD - APE 5/20/2004
#delete unencrypted local archive copy
#@filelist = ("$workingDir$archive_filename");
#unlink @filelist;
#print LOG_FILE "Finished Removing unencrypted files.";
print LOG_FILE "Removing d4 XTRDWLDINT.txt file.";
unlink $rootDir."FTP\\d4\\outgoing\\XTRDWLDINT.txt";
print LOG_FILE "Finished Removing d4 XTRDWLDINT.txt file.";

close (LOG_FILE);

#Exit cleanly
exit;

sub CreateTimeStamp
	{
		# Get information to produce the name of the files
		($year)=((localtime)[5]) + 1900;
		($month) = ((localtime)[4]) + 1;
		if (length($month) == 1){$month = "0" . $month;} # pad single digit month
		($day) = ((localtime)[3]);
		if (length($day) == 1){$day = "0" . $day;} # pad single digit day
		($hour) = ((localtime)[2]);
		if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
		($min) = ((localtime)[1]);
		if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
		($sec) = ((localtime)[0]);
		if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
		return $year, $month, $day, $hour, $min, $sec;
	}
	
sub encryptStuff {
	system "gpg --homedir $homedir --output $encryptedfile --recipient hghavami\@usatoday.com --encrypt $filetoencrypt";
}

sub sendftp {
	$filetypeswitch= "binary";
	#set as default

	my $ftp;
	
	if (my($ftp) = Net::FTP->new("$server",Passive => 1)) {
	   if($ftp) {
		$ftp->debug("$debug");
		$ftp->login("$login","$pass") or
			die "Failed to log into server $server";
			#or 
			#	($ftpstep = $bad and $msg = "$fail_login"."$login"."/"."$pass") if $ftpstep eq $good;
		if ($filetypeswitch eq "ascii") {
				$ftp->ascii();
		} else {
				$ftp->binary();	
		}
		
		if ($todir eq "root") {
			#no action required
			allmessages("Put file to $todir directory \n");
		} else {
			allmessages("Changing to directory $todir \n");
			$ftp->cwd("$todir");
		}
		
		$ftp->put("$fileToSend");
			# if $ftpstep eq $good;
		
		$ftp->quit();

   		$msg = "FTP over the WAN/VPN from $encryptedfile  to $server as $filetypeswitch successful";
		allmessages($msg."\n");

	   } else {
		
		$processed = $bad and $ftpstep = $bad and $msg = "$fail_connect"."$server";
		allmessages($msg."\n");	
	   }
	
 	} else {

       	$processed = $bad and $ftpstep = $bad and $msg = "$fail_connect"."$server";
		allmessages($msg."\n");	
	}
}

#*********************************************************************
#** SUBROUTINE THAT SENDS A FILE VIA SFTP.
#*********************************************************************
sub send_sFTP
{

  my $system   = $sserver;
  my $login    = $slogin;
  my $password = $spass;
  $todir = $stodir;    # directory on remote server
  my $filename = $extract_file_name;
  my $InputDir = $workingDir;

  my $port = $sport;

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;

  $sftp = new chilkat::CkSFtp();


  # The filename is passed with the full path.
  #
  my($FileUtilHandle) = File::Util->new();
  my $short_filename = $FileUtilHandle->strip_path($filename);

  #  Any string automatically begins a fully-functional 30-day trial.
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " connected to $system\n";
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.  This example uses password authenication.
  $success = $sftp->AuthenticatePw( "$login", "$password" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system\n";
    $ErrorMsg = &now . " Could not log on to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " logged on to $system\n";
  }

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Initialized SFTP subsystem\n";
  }

  #  To find the full path of our user account's home directory,
  #  call RealPath like this:

  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  #  Open a file for writing on the SSH server.
  #  If the file already exists, it is overwritten.
  #  (Specify "createNew" instead of "openOrCreate" to
  #  prevent overwriting existing files.)

  print "remote file: $absPath" . "/" . "$todir$short_filename\n";
  # print "remote file: $absPath" . "/" . "$todir$filename\n";
  $handle = $sftp->openFile( "$absPath" . "/" . "$todir$short_filename", "writeOnly", "openOrCreate" );
  # $handle = $sftp->openFile( "$absPath" . "/" . "$todir$filename", "writeOnly", "openOrCreate" );

  # if ( $handle eq null )
  if ( $handle eq "" )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not open file in remote system\n";
    $ErrorMsg = &now . " Could not open file in remote system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Opened remote file: $absPath$todir$filename\n";
  }

  #  Upload from the local file to the SSH server.
  $success = $sftp->UploadFile( $handle, "$filename" );
  # $success = $sftp->UploadFile( $handle, "$InputDir$filename" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not upload file to remote system\n";
    $ErrorMsg = &now . " Could not upload file to remote system\n";
    return "0|$ErrorMsg";
  }

  #  Close the file.
  $success = $sftp->CloseHandle($handle);
  if ( $success != 1 )
  {
    print "closehandle: " . $sftp->lastErrorText() . "\n";
    exit;
  }

  print "Success." . "\n";

  
} # sub send_sFTP


sub allmessages {
	print  LOG_FILE "$_[0]";
}

#*********************************************************************
#** SUBROUTINE THAT PRINTS THE TIME.
#*********************************************************************
#
sub now
	{
		time2str("%T ", time)
	}



sub send_alert
	{
		foreach $SendTo (@notify)
			{
				$msg = $_[0];
				$subject = $_[1];

				$smtp = Net::SMTP->new($mailServer); 	# connect to an SMTP server
				$smtp->mail( 'XTRNTDWLD_PERL_d4@usatoday.com' );     		# use the sender's address here
				$smtp->to($SendTo);        				# recipient's address

				$smtp->data();                      			# Start the mail

				# Send the header.
				$smtp->datasend("To: $SendTo\n");
				$smtp->datasend("From: staging server d4\n");
				$smtp->datasend("Subject: $subject\n");
				$smtp->datasend("\n");

				# Send the body.
				$smtp->datasend($msg);
				$smtp->dataend();                   # Finish sending the mail
				$smtp->quit;                        # Close the SMTP
				

				print LOG_FILE $timestamp . " -- email/page sent to: $SendTo\n";
			}	
	}
