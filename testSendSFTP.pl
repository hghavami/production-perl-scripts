use Win32::ODBC;
use Net::SMTP;
use File::Copy;
use File::stat;
use File::Util;
use Net::FTP;
use Win32::Process;
use Date::Format;
use chilkat;


# Configure following based on prod/test environment
my $slogin = "usat-webportaltest";		# sftp login
my $spass = "pBIsm9vS5JSiZsGFpecr";		# sftp password
my $sserver = "ftps.gannett.com";		# sftp server
my $sport = "22";						# sftp port

my $todir = "incoming";  				# destination server directory to put file
my $stodir = "/incoming/";
my $rootDir = "L:\\USAT\\";
my $workingDir = $rootDir . "processed\\archive\\";

my $extract_file_name = "$workingDir" . "XD07131245" . ".gpg";

send_sFTP();

#*********************************************************************
#** SUBROUTINE THAT SENDS A FILE VIA SFTP.
#*********************************************************************
sub send_sFTP
{

  my $system   = $sserver;
  my $login    = $slogin;
  my $password = $spass;
  $todir = $stodir;    # directory on remote server
  my $filename = $extract_file_name;
  my $InputDir = $workingDir;

  my $port = $sport;

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;

  $sftp = new chilkat::CkSFtp();


  # The filename is passed with the full path.
  #
  my($FileUtilHandle) = File::Util->new();
  my $short_filename = $FileUtilHandle->strip_path($filename);

  #  Any string automatically begins a fully-functional 30-day trial.
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " connected to $system\n";
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.  This example uses password authenication.
  $success = $sftp->AuthenticatePw( "$login", "$password" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system\n";
    $ErrorMsg = &now . " Could not log on to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " logged on to $system\n";
  }

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Initialized SFTP subsystem\n";
  }

  #  To find the full path of our user account's home directory,
  #  call RealPath like this:

  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  #  Open a file for writing on the SSH server.
  #  If the file already exists, it is overwritten.
  #  (Specify "createNew" instead of "openOrCreate" to
  #  prevent overwriting existing files.)

  print "remote file: $absPath" . "/" . "$todir$short_filename\n";
  # print "remote file: $absPath" . "/" . "$todir$filename\n";
  $handle = $sftp->openFile( "$absPath" . "/" . "$todir$short_filename", "writeOnly", "openOrCreate" );
  # $handle = $sftp->openFile( "$absPath" . "/" . "$todir$filename", "writeOnly", "openOrCreate" );

  # if ( $handle eq null )
  if ( $handle eq "" )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not open file in remote system\n";
    $ErrorMsg = &now . " Could not open file in remote system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Opened remote file: $absPath$todir$filename\n";
  }

  #  Upload from the local file to the SSH server.
  $success = $sftp->UploadFile( $handle, "$filename" );
  # $success = $sftp->UploadFile( $handle, "$InputDir$filename" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not upload file to remote system\n";
    $ErrorMsg = &now . " Could not upload file to remote system\n";
    return "0|$ErrorMsg";
  }

  #  Close the file.
  $success = $sftp->CloseHandle($handle);
  if ( $success != 1 )
  {
    print "closehandle: " . $sftp->lastErrorText() . "\n";
    exit;
  }

  print "Success." . "\n";

  return 1;
} # sub send_sFTP

#*********************************************************************
#** SUBROUTINE THAT PRINTS THE TIME.
#*********************************************************************
#
sub now
	{
		time2str("%T ", time)
	}

return 1;

