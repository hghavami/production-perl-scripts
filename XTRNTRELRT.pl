#################################################################################
#
# XTRNTRELRT.PL
#
# Created by Scott Seller
#
# updated 8/6/2002 Scott Seller Added code to check for a zero length file.
#                               Added send_alert subroutine.
# updated 05/22/2007 Houman Ghavami - Added an error email counter.  Only send 5 email errors
#								Successful exectution requires notify.txt be in the same directory as the script
#
# A seperate process (FILE_SCAN tool) monitors the directory and runs this script.
#
#################################################################################
use Win32::ODBC;
use Net::SMTP;
use File::stat;

my($db) = new Win32::ODBC("dsn=esub; uid=usatSPODBC; pwd=subscribe");

CreateTimeStamp();

$db->SetConnectOption('SQL_AUTOCOMMIT', 'SQL_AUOTCOMMIT_OFF');

$ACTIVE = "N";
$file_size = 0;
$ErrorCounter = 0;
$LogFileName = "xtrntrelrt_" . $year . $month . $day . ".log";
$timestamp = localtime;

$mailServer = "smtp.v4.gmti.gbahn.net";

open (LOG_FILE, ">>L:\\USAT\\processed\\logs\\$LogFileName");

### Create email/page list

if (open(NL, "L:\\USAT\\scripts\\notify_list.txt"))
	{
		@notify = (<NL>);
		close NL;
	}
else
	{
		print "Error opening notify_list.txt\n";
		print LOG_FILE "$timestamp -- Error opening notify_list.txt\n";
		die;
	}	

### Check to make sure we didn't get an empty file

$file_size = stat("L:\\USAT\\FTP\\incoming\\XTRNTRELRT")->size;
if ($file_size == 0)
	{
		print LOG_FILE "$timestamp -- XTRNTRELRT is empty.\n";
		&send_alert("$timestamp -- XTRNTRELRT is empty.\n", "XTRNTRELRT Was Empty from 400");
		rename ("L:\\USAT\\FTP\\incoming\\XTRNTRELRT", "L:\\USAT\\processed\\XTRNTRELRT");
		die;
	}
	

open (EXTRACT_FILE, "L:\\USAT\\FTP\\incoming\\XTRNTRELRT");
$Start_time = localtime;
print LOG_FILE "Started processing XTRNTRELRT at $Start_time\n";
print "Started processing XTRNTRELRT at $Start_time\n";

while (<EXTRACT_FILE>)
	{
		$_ =~ s/\'/\`/g;
		$_ =~ s/\,/ /g;
		$_ =~ s/\"/\`\`/g;
		$_ =~ s/\x00/\x20/g;
		chomp;
		$krpub = substr($_,0,2);
		$krfod = substr($_,2,2);
		$krdelm = substr($_,4,1);
		$krprcd = substr($_,5,2);
		$krprcr= substr($_,7,2);
		$krdur = substr($_,9,3);
		$kramt = substr($_,12,5);
		$krdesc = substr($_,17,50);
		$krnewr = substr($_,67,2);
		$krnewl = substr($_,69,3);
		$krnewv= substr($_,72,5);
		$krnewd = substr($_,77,50);

		# print "$krdesc\n";
		
		if ($db->Sql("INSERT INTO XTRNTRELRT 
					(Pubcode, FreqofDelivery, DelMethod, PiaRateCode, RelRateCode, 
					RelPeriodLength, RelOfferAmount, RelOfferDesc, RenRateCode, RenPeriodLength, 
					RenOfferAmount, RenOfferDesc, ACTIVE) 
					VALUES 
					('$krpub', '$krfod', '$krdelm', '$krprcd', '$krprcr', '$krdur', '$kramt', 
					'$krdesc', '$krnewr', '$krnewl', '$krnewv', '$krnewd', '$ACTIVE')"))
			{
			#$db->Transact('SQL_ROLLBACK');
			my($err) = $db->Error;
			warn "Sql() ERROR\n";
			warn "\t\$stmt: INSERT INTO XTRNTRELRT...\n";
			warn "\t\$err: $err\n";
			# send an email
			$eMsg = "INSERT INTO XTRNTRELRT 
					(Pubcode, FreqofDelivery, DelMethod, PiaRateCode, RelRateCode, 
					RelPeriodLength, RelOfferAmount, RelOfferDesc, RenRateCode, RenPeriodLength, 
					RenOfferAmount, RenOfferDesc, ACTIVE) 
					VALUES 
					('$krpub', '$krfod', '$krdelm', '$krprcd', '$krprcr', '$krdur', '$kramt', 
					'$krdesc', '$krnewr', '$krnewl', '$krnewv', '$krnewd', '$ACTIVE')";
                        # Issue up to 5 error emails
                        if ($ErrorCounter < 5) {
				&send_alert("$timestamp -- \n\nError Code: $err\n\nApplication Terminated. Manual Intervention required.\n\nSQL Statement: $eMsg", "Database error Inserting into XTRNTRELRT");
                                $ErrorCounter++;
                        }			

			exit;
		}	

} # while (<EXTRACT_FILE>)

# $db->Transact('SQL_COMMIT');

# Remove old records
$db->Sql("DELETE FROM XTRNTRELRT WHERE ACTIVE = 'Y'"); 
# Update newly inserted records.
$db->Sql("UPDATE XTRNTRELRT SET ACTIVE = 'Y'"); 

$db->Close();
$end_time = localtime;
print LOG_FILE "Finished processing XTRNTRELRT at $end_time\n";
close (EXTRACT_FILE);
close (LOG_FILE);

$timestamp = join('_', split(/:/, localtime));
# $timestamp =~ join('_', split(/ /, $timestamp));
$new_name = "XTRNTRELRT_" . $timestamp;
rename ("L:\\USAT\\FTP\\incoming\\XTRNTRELRT", "L:\\USAT\\processed\\$new_name");


sub CreateTimeStamp
	{
		# ***********************************************************************
		# Get information to produce the name of the files
		($year)=((localtime)[5]) + 1900;
		($month) = ((localtime)[4]) + 1;
		if (length($month) == 1){$month = "0" . $month;} # pad single digit month
		($day) = ((localtime)[3]);
		if (length($day) == 1){$day = "0" . $day;} # pad single digit day
		($hour) = ((localtime)[2]);
		if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
		($min) = ((localtime)[1]);
		if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
		($sec) = ((localtime)[0]);
		if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
		# ************************************************************************
		return $year, $month, $day, $hour, $min, $sec;
	}

sub send_alert
	{
		foreach $SendTo (@notify)
			{
				$msg = $_[0];
				$subject = $_[1];

				$smtp = Net::SMTP->new($mailServer); 	# connect to an SMTP server
				$smtp->mail( 'XTRNTRELRT_PERL@usatoday.com' );     		# use the sender's address here
				$smtp->to($SendTo);        				# recipient's address

				$smtp->data();                      			# Start the mail

				# Send the header.
				$smtp->datasend("To: $SendTo\n");
				$smtp->datasend("From: moc-wn1048\n");
				$smtp->datasend("Subject: $subject\n");
				$smtp->datasend("\n");

				# Send the body.
				$smtp->datasend($msg);
				$smtp->dataend();                   # Finish sending the mail
				$smtp->quit;                        # Close the SMTP
				

				print LOG_FILE $timestamp . " -- email/page sent to: $SendTo\n";
			}	
	}
