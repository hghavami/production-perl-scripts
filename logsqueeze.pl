# ######################################################################### 
# 
# Filename: LogSqueeze.pl
# 
# Created: 18 November 2002
# 
# Author: Scott Seller, USA TODAY
# 
# The first argument is the directory to process. This directory 
# must be specified as an absolute path from the root.
# 
# 
# The syntax for cleanup is: LogSqueeze [directory to process]
# 
# 86400 seconds in a day
# 604800 seconds in a week.
# 
# #########################################################################
use Archive::Zip;
use POSIX qw/strftime/; 


$directory_to_process = ($ARGV[0]);
# print substr($directory_to_process,0,3)."\n";

unless (chdir $directory_to_process) 
	{
#		print LOG_FILE "$CurrentTime -- Cannot change to the specified directory: $directory_to_process\n";
		die "Cannot change to the specified directory: $directory_to_process";
	};
opendir(test_dir,$directory_to_process);
@dir_files = grep -f,readdir(test_dir); # only grab files, not directories
closedir(test_dir);

# print @dir_files;

foreach my $log (@dir_files) {

 	if ($log =~ m/esub_ssl/)
		{ 
			my @date = ($log =~ m/(\d+)$/);
			$date= $date[0];
			my $fmtStr = strftime "%Y%m%d", localtime($date); 
			my $zip = Archive::Zip->new(); 
			$zip->addFile ($log); 
			$zip->writeToFileNamed("C:\\USAT\\FTP\\toDotCom\\esub_ssl_access.$fmtStr.log.zip");
			unlink $log;
		}
}
