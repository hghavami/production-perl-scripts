#################################################################################
#
# ZIP5GUI.PL
#
# Created 12/2000 by Houman Ghavami
#
# updated 3/1/2004 Houman Ghavami Created Perl script
#								Successful exectution requires notify.txt be in the same directory as the script
#
# A seperate process (FILE_SCAN tool) monitors the directory and runs this script.
#
#################################################################################
use Win32::ODBC;
use Net::SMTP;
use File::stat;


my($db) = new Win32::ODBC("dsn=esub; uid=usatSPODBC; pwd=subscribe");

$db->SetConnectOption('SQL_AUTOCOMMIT', 'SQL_AUOTCOMMIT_OFF');

&CreateTimeStamp();

$ACTIVE = "N";
$counter = 0;
$err_counter = 0;
$file_size = 0;
$LogFileName = "ZIP5GUI_" . $year . $month . $day . ".log";
$timestamp = localtime;

$mailServer = "smtp.v4.gmti.gbahn.net";

open (LOG_FILE, ">>L:\\USAT\\processed\\logs\\$LogFileName");

### Create email/page list

if (open(NL, "L:\\USAT\\scripts\\notify_list.txt"))
	{
		@notify = (<NL>);
		close NL;
	}
else
	{
		print "Error opening notify_list.txt\n";
		print LOG_FILE "$timestamp -- Error opening notify_list.txt\n";
		die;
	}	

### Check to make sure we didn't get an empty file

$file_size = stat("L:\\USAT\\FTP\\incoming\\ZIP5GUI")->size;
if ($file_size == 0)
	{
		print LOG_FILE "$timestamp -- ZIP5GUI is empty.\n";
		&send_alert("$timestamp -- ZIP5GUI is empty.\n", "ZIP5GUI is empty");
		rename ("L:\\USAT\\FTP\\incoming\\ZIP5GUI", "L:\\USAT\\processed\\ZIP5GUI");
		die;
	}

open (EXTRACT_FILE, "L:\\USAT\\FTP\\incoming\\ZIP5GUI");
print LOG_FILE "$timestamp -- Started processing ZIP5GUI\n";
print "$timestamp -- Started processing ZIP5GUI\n";

while (<EXTRACT_FILE>)
	{
		print ".";
		$PubCode = substr($_,0,2);
		$ZipCode = substr($_,2,5);

		$counter++;
		
		if ($db->Sql("INSERT INTO ZIP5GUI 
					(PubCode, ZipCode, ACTIVE) 
					VALUES 
					('$PubCode', '$ZipCode', '$ACTIVE')"))
			{
			$err_counter++;
			#$db->Transact('SQL_ROLLBACK');
			my($err) = $db->Error;
			warn "Sql() ERROR\n";
			warn "\t\$stmt: INSERT INTO ZIP5GUI...\n";
			warn "\t\$err: $err\n";
			# send an email
			
			$eMsg = "pubcode=$PubCode, zipcode=$ZipCode, Active=$ACTIVE";

			&send_alert("$timestamp -- \n\nError Code: $err\n\nApplication Terminated. Manual intervention reuired.\n\nDATA: $eMsg", "INSERT INTO ZIP5GUI FAILED");
									
			exit;
		}	

} # while (<EXTRACT_FILE>)

# $db->Transact('SQL_COMMIT');

# Remove old records

$timestamp = localtime;
print LOG_FILE "$timestamp -- Deleting old records\n";
print "$timestamp -- Deleting old records\n";
$db->Sql("DELETE FROM ZIP5GUI WHERE ACTIVE = 'Y'"); 

# Update newly inserted records.
$timestamp = localtime;
print LOG_FILE "$timestamp -- setting active flag to Y\n";
print "$timestamp -- setting active flag to Y\n";
$db->Sql("UPDATE ZIP5GUI SET ACTIVE = 'Y'"); 

$db->Close();
$end_time = localtime;
print LOG_FILE "$counter records inserted\n";
print LOG_FILE "$err_counter records not processed\n";
print LOG_FILE "$end_time -- Finished processing ZIP5GUI\n";
print LOG_FILE "-----------------------------\n";
close (EXTRACT_FILE);
close (LOG_FILE);

$timestamp = join('_', split(/:/, localtime));
# $timestamp =~ join('_', split(/ /, $timestamp));
$new_name = "ZIP5GUI_" . $timestamp;
rename ("L:\\USAT\\FTP\\incoming\\ZIP5GUI", "L:\\USAT\\processed\\$new_name");

sub send_alert
	{
		foreach $SendTo (@notify)
			{
				$msg = $_[0];
				$subject = $_[1];

				$smtp = Net::SMTP->new($mailServer); 	# connect to an SMTP server
				$smtp->mail( 'ZIP5GUI_PERL@usatoday.com' );     		# use the sender's address here
				$smtp->to($SendTo);        				# recipient's address

				$smtp->data();                      			# Start the mail

				# Send the header.
				$smtp->datasend("To: $SendTo\n");
				$smtp->datasend("From: moc-wn1048\n");
				$smtp->datasend("Subject: $subject\n");
				$smtp->datasend("\n");

				# Send the body.
				$smtp->datasend($msg);
				$smtp->dataend();                   # Finish sending the mail
				$smtp->quit;                        # Close the SMTP
				

				print LOG_FILE $timestamp . " -- email/page sent to: $SendTo\n";
			}	
	}



sub CreateTimeStamp
	{
		# Get information to produce the name of the files
		($year)=((localtime)[5]) + 1900;
		($month) = ((localtime)[4]) + 1;
		if (length($month) == 1){$month = "0" . $month;} # pad single digit month
		($day) = ((localtime)[3]);
		if (length($day) == 1){$day = "0" . $day;} # pad single digit day
		($hour) = ((localtime)[2]);
		if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
		($min) = ((localtime)[1]);
		if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
		($sec) = ((localtime)[0]);
		if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
		return $year, $month, $day, $hour, $min, $sec;
	}