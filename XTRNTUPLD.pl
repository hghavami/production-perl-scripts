#################################################################################
#
# XTRNTUPLD.PL
#
# Created 12/20/2000 by Scott Seller
#
# updated 4/18/2001 Scott Seller
# updated 3/1/2002 Scott Seller
#		Added three new fields for renew to functions
#			RateCode,C,2
#			RenewalTypeAlpha,C,1
#			PerpCCFlag,C,1
# updated 7/11/2002 Scott Seller Added more information to log files and added
#				 a status indicator while processing
#
# updated 8/6/2002 Scott Seller Added code to check for a zero length file.
#                               Added send_alert subroutine.
#								Successful exectution requires notify.txt be in the same directory as the script
# Updated 10/19/2004 Houman Ghavami Added fields VacRestart and LastStopCode to the file
# updated 10/27/2006 Houman Ghavami - Added delivery method and removed unused fields.
# updated 04/16/2007 Houman Ghavami - Added an error email counter.  Only send 5 email errors
# Weekly processing of updates from the AS/400
#
# Replaces all records in XTRNTUPLD.
# This script is triggered to run when XTRNTUPLD appears in /transfer/incoming.
#
# A seperate process (FILE_SCAN tool) monitors the directory and runs this script.
#
#################################################################################
use Win32::ODBC;
use Net::SMTP;
use File::stat;

my($db) = new Win32::ODBC("dsn=esub; uid=usatSPODBC; pwd=subscribe");

$mailServer = "smtp.v4.gmti.gbahn.net";

# ***********************************************************************
# Get information to produce the name of the files
($year)=((localtime)[5]) + 1900;
($month) = ((localtime)[4]) + 1;
if (length($month) == 1){$month = "0" . $month;} # pad single digit month
($day) = ((localtime)[3]);
if (length($day) == 1){$day = "0" . $day;} # pad single digit day
($hour) = ((localtime)[2]);
if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
($min) = ((localtime)[1]);
if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
($sec) = ((localtime)[0]);
if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
# ************************************************************************

$ACTIVE = "N";
$counter = 0;
$ErrorCounter = 0;
$file_size = 0;
$LogFileName = "xtrntupw_" . $year . $month . $day . ".log";
$timestamp = localtime;


open (LOG_FILE, ">>L:\\USAT\\processed\\logs\\$LogFileName");

### Create email/page list

if (open(NL, "L:\\USAT\\scripts\\notify_list.txt"))
	{
		@notify = (<NL>);
		close NL;
	}
else
	{
		print "Error opening notify_list.txt\n";
		print LOG_FILE "$timestamp -- Error opening notify_list.txt\n";
		die;
	}	

### Check to make sure we didn't get an empty file

$file_size = stat("L:\\USAT\\FTP\\incoming\\XTRNTUPW")->size;
if ($file_size == 0)
	{
		print LOG_FILE "$timestamp -- XTRNTUPW is empty.\n";
		&send_alert("$timestamp -- XTRNTUPW is empty.\n", "XTRRNTUPW was empty from 400");
		rename ("L:\\USAT\\FTP\\incoming\\XTRNTUPW", "L:\\USAT\\processed\\XTRNTUPW");
		die;
	}
	
open (EXTRACT_FILE, "L:\\USAT\\FTP\\incoming\\XTRNTUPW");

# $Start_time = localtime;

$timestamp = localtime;
print { $OK ? "STDOUT" : "LOG_FILE" } "$timestamp -- Started processing XTRNTUPW\n";

while (my $current_line = <EXTRACT_FILE>)
	{
		print ".";
		$counter++;
		$current_line =~ s/\'/\`/g;
		$current_line =~ s/\,/ /g;
		$current_line =~ s/\x00/\x20/g;
		chomp $current_line;
		$PubCode = substr($current_line,0,2);
		$TransDate = substr($current_line,2,8);
		$TransRecType = substr($current_line,10,2);
		$LastName = substr($current_line,12,15);
		$FirstName= substr($current_line,27,10);
		$FirmName = substr($current_line,37,28);
		$AddlAddr1= substr($current_line,65,28);
		$AddlAddr2= substr($current_line,93,28);
		$HalfUnitNum = substr($current_line,121,3);
		$StreetDir= substr($current_line,124,2);
		$StreetName = substr($current_line,126,28);
		$StreetType = substr($current_line,154,4);
		$StreetPostDir = substr($current_line,158,2);
		$SubUnitCode = substr($current_line,160,4);
		$SubUnitNum = substr($current_line,164,8);
		$City = substr($current_line,172,28);
		$State = substr($current_line,200,2);
		$Zip = substr($current_line,202,5);
		$HomePhone = substr($current_line,207,10);
		$BusPhone = substr($current_line,217,10);
		$CurrAcctNum = "34" . substr($current_line,227,7); # APPEND "34" TO ALL ACCOUNTS TO MATCH DATABASE
                $DeliveryMethod = substr($current_line,234,1);
		$PromoCode = substr($current_line,235,2);
		$NumPapers = substr($current_line,237,3);
		$ContestCode = substr($current_line,240,2);
		$SrcOrderCode = substr($current_line,242,1);
		$Duration = substr($current_line,243,3);
		$LastPayRecvd = substr($current_line,246,7);
		$PIAPermStartDate = substr($current_line,253,7);
		$PIAExpirationDate = substr($current_line,260,7);
		$BillLastName = substr($current_line,267,15);
		$BillFirstName = substr($current_line,282,10);
		$BillFirmName = substr($current_line,292,28);
		$BillAddr1 = substr($current_line,320,28);
		$BillAddr2 = substr($current_line,348,28);
		$BillUnitNum = substr($current_line,376,10);
		$BillHalfUnitNum = substr($current_line,386,3);
		$BillStreetDir = substr($current_line,389,2);
		$BillStreetName = substr($current_line,391,28);
		$BillStreetType = substr($current_line,419,4);
		$BillStreetPostDir = substr($current_line,423,2);
		$BillSubunitNum = substr($current_line,425,8);
		$BillSubunitCode = substr($current_line,433,4);
		$BillCity = substr($current_line,437,28);
		$BillState= substr($current_line,465,2);
		$BillZip5 = substr($current_line,467,5);
		$BillBusPhone = substr($current_line,472,10);
		$BillHomePhone = substr($current_line,482,10);
		$CurrentAccountBalance = substr($current_line,492,11);
		$UnitNum = substr($current_line,503,10);
		$OneTimeBill = substr($current_line,513,1);
		$RateCode = substr($current_line,514,2);
		$RenewalTypeAlpha = substr($current_line,516,1);
		$PerpCCFlag = substr($current_line,517,1);
                $VacRestart = substr($current_line,518,1);
                $LastStopCode = substr($current_line,519,2);
                $CreditCardNum = substr($current_line,521,4);
                $CreditCardType = substr($current_line,525,1);
                $CreditCardExp = substr($current_line,526,4);

		
		if ($db->Sql("INSERT INTO XTRNTUPLD
					(PubCode,TransDate,TransRecType,LastName,FirstName,FirmName,AddlAddr1,AddlAddr2,
					HalfUnitNum,StreetDir,StreetName,StreetType,StreetPostDir,SubUnitCode,SubUnitNum,City,State,Zip,HomePhone,
					BusPhone,CurrAcctNum,DeliveryMethod,PromoCode,NumPapers,ContestCode,SrcOrderCode,Duration,
					LastPayRecvd,PIAPermStartDate,PIAExpirationDate,BillLastName,BillFirstName,BillFirmName,BillAddr1,
					BillAddr2,BillUnitNum,BillHalfUnitNum,BillStreetDir,BillStreetName,BillStreetType,BillStreetPostDir,
					BillSubunitNum,BillSubunitCode,BillCity,BillState,BillZip5,BillBusPhone,BillHomePhone,CurrentAccountBalance,
					UnitNum,ACTIVE,OneTimeBill,RateCode,RenewalTypeAlpha,PerpCCFlag,VacRestart,LastStopCode,CreditCardNum,
                              CreditCardType,CreditCardExp)
					VALUES
					('$PubCode','$TransDate','$TransRecType','$LastName','$FirstName','$FirmName','$AddlAddr1','$AddlAddr2',
					'$HalfUnitNum','$StreetDir','$StreetName','$StreetType','$StreetPostDir','$SubUnitCode','$SubUnitNum',
					'$City','$State','$Zip','$HomePhone','$BusPhone','$CurrAcctNum','$DeliveryMethod','$PromoCode',
					'$NumPapers','$ContestCode','$SrcOrderCode','$Duration','$LastPayRecvd','$PIAPermStartDate',
					'$PIAExpirationDate','$BillLastName',
					'$BillFirstName','$BillFirmName','$BillAddr1','$BillAddr2','$BillUnitNum','$BillHalfUnitNum',
					'$BillStreetDir','$BillStreetName','$BillStreetType','$BillStreetPostDir','$BillSubunitNum',
					'$BillSubunitCode','$BillCity','$BillState','$BillZip5','$BillBusPhone','$BillHomePhone',
					'$CurrentAccountBalance','$UnitNum','$ACTIVE','$OneTimeBill','$RateCode','$RenewalTypeAlpha',
                              '$PerpCCFlag','$VacRestart','$LastStopCode','$CreditCardNum','$CreditCardType','$CreditCardExp')"))
			{
			# $db->Transact('SQL_ROLLBACK');
			$timestamp = localtime;
			my($err) = $db->Error;
			print LOG_FILE "$timestamp -- Sql() ERROR\n";
			print LOG_FILE "$timestamp -- \t\$stmt: INSERT INTO XTRNTUPLD...\n";
			print LOG_FILE "$timestamp -- \t\$err: $err\n";
			warn "Sql() ERROR\n";
			warn "\t\$stmt: INSERT INTO XTRNTUPLD...\n";
			warn "\t\$err: $err\n";
			# ODBC drivers Do not support Rollback. We have to delete the inserted records manualy
			# $db->Sql("DELETE FROM XTRNTUPLD WHERE ACTIVE = 'N'");
			# send an email
			$eMsg = "INSERT INTO XTRNTUPLD
					(PubCode,TransDate,TransRecType,LastName,FirstName,FirmName,AddlAddr1,AddlAddr2,
					HalfUnitNum,StreetDir,StreetName,StreetType,StreetPostDir,SubUnitCode,SubUnitNum,City,State,Zip,HomePhone,
					BusPhone,CurrAcctNum,DeliveryMethod,PromoCode,NumPapers,ContestCode,SrcOrderCode,Duration,
					LastPayRecvd,PIAPermStartDate,PIAExpirationDate,BillLastName,BillFirstName,BillFirmName,BillAddr1,
					BillAddr2,BillUnitNum,BillHalfUnitNum,BillStreetDir,BillStreetName,BillStreetType,BillStreetPostDir,
					BillSubunitNum,BillSubunitCode,BillCity,BillState,BillZip5,BillBusPhone,BillHomePhone,CurrentAccountBalance,
					UnitNum,ACTIVE,OneTimeBill,RateCode,RenewalTypeAlpha,PerpCCFlag,VacRestart,LastStopCode,CreditCardNum,
                              CreditCardType,CreditCardExp)
					VALUES
					('$PubCode','$TransDate','$TransRecType','$LastName','$FirstName','$FirmName','$AddlAddr1','$AddlAddr2',
					'$HalfUnitNum','$StreetDir','$StreetName','$StreetType','$StreetPostDir','$SubUnitCode','$SubUnitNum',
					'$City','$State','$Zip','$HomePhone','$BusPhone','$CurrAcctNum','$DeliveryMethod','$PromoCode',
					'$NumPapers','$ContestCode','$SrcOrderCode','$Duration','$LastPayRecvd','$PIAPermStartDate',
					'$PIAExpirationDate','$BillLastName',
					'$BillFirstName','$BillFirmName','$BillAddr1','$BillAddr2','$BillUnitNum','$BillHalfUnitNum',
					'$BillStreetDir','$BillStreetName','$BillStreetType','$BillStreetPostDir','$BillSubunitNum',
					'$BillSubunitCode','$BillCity','$BillState','$BillZip5','$BillBusPhone','$BillHomePhone',
					'$CurrentAccountBalance','$UnitNum','$ACTIVE','$OneTimeBill','$RateCode','$RenewalTypeAlpha',
                              '$PerpCCFlag','$VacRestart','$LastStopCode','$CreditCardNum','$CreditCardType','$CreditCardExp')";

                        # Issue up to 5 error emails
                        if ($ErrorCounter < 5) {
        			&send_alert("$timestamp -- \n\nError Code: $err\n\nSQL Statement: $eMsg", "Database error Inserting into XTRNTUPLD");
                                $ErrorCounter++;
                        }
                        # 5 errors? then exit
                        if ($ErrorCounter == 5) {
                                $exit;
                        }
			$textstring = "$PubCode" . "$TransDate" . "$TransRecType" . "$LastName" . "$FirstName" . "$FirmName" . "$AddlAddr1" . "$AddlAddr2" .
					"$HalfUnitNum" . "$StreetDir" . "$StreetName" . "$StreetType" . "$StreetPostDir" . "$SubUnitCode" . "$SubUnitNum" .
					"$City" . "$State" . "$Zip" . "$HomePhone" . "$BusPhone" . "$CurrAcctNum" . "DeliveryMethod" . "$PromoCode" .
					"$NumPapers" . "$ContestCode" . "$SrcOrderCode" . "$Duration" . "$LastPayRecvd" . "$PIAPermStartDate" .
					"$PIAExpirationDate" . "$BillLastName" .
					"$BillFirstName" . "$BillFirmName" . "$BillAddr1" . "$BillAddr2" . "$BillUnitNum" . "$BillHalfUnitNum" .
					"$BillStreetDir" . "$BillStreetName" . "$BillStreetType" . "$BillStreetPostDir" . "$BillSubunitNum" .
					"$BillSubunitCode" . "$BillCity" . "$BillState" . "$BillZip5" . "$BillBusPhone" . "$BillHomePhone" .
					"$CurrentAccountBalance" . "$UnitNum" . "$ACTIVE" ."$OneTimeBill" ."$RateCode" ."$RenewalTypeAlpha" ."$PerpCCFlag" .
					"$VacRestart" . "$LastStopCode" . "$CreditCardNum" . "$CreditCardType" . "$CreditCardExp\n";

			print LOG_FILE "error number: $err\n";
			print LOG_FILE $textstring;		
			# exit;
			} # if ($db->Sql("INSERT....

			if ($counter%1000 == 0)
				{
					print "\nRecords processed so far: $counter\n";
				}

} # while (<EXTRACT_FILE>)

# $db->Transact('SQL_COMMIT');

# Remove records if less than 5 errors
if ($ErrorCounter < 5) {
        # Remove old records
        $timestamp = localtime;
        print { $OK ? "STDOUT" : "LOG_FILE" } "\n$timestamp -- Removing old records from database.\n";
        $db->Sql("DELETE FROM XTRNTUPLD WHERE ACTIVE = 'Y'");

        # Update newly inserted records.
        $timestamp = localtime;
        print { $OK ? "STDOUT" : "LOG_FILE" }  "\n$timestamp -- Setting active flag to Y.\n";
        $db->Sql("UPDATE XTRNTUPLD SET ACTIVE = 'Y'");
}
# If error count = 5, then remove newly added records
else {
        # Remove new records
        $timestamp = localtime;
        print { $OK ? "STDOUT" : "LOG_FILE" } "\n$timestamp -- Too many errors, removing new records from database.\n";
        $db->Sql("DELETE FROM XTRNTUPLD WHERE ACTIVE = 'N'");
}

$db->Close();
$timestamp = join('_', split(/:/, localtime));
# $end_time = localtime;
print LOG_FILE "$timestamp -- Processed $counter records\n";
print LOG_FILE "$timestamp -- Finished processing XTRNTUPW.\n";
close (EXTRACT_FILE);
close (LOG_FILE);

$new_name = "XTRNTUPW_" . $timestamp;

rename ("L:\\USAT\\FTP\\incoming\\XTRNTUPW", "L:\\USAT\\processed\\$new_name");

sub CreateTimeStamp
	{
		# ***********************************************************************
		# Get information to produce the name of the files
		($year)=((localtime)[5]) + 1900;
		($month) = ((localtime)[4]) + 1;
		if (length($month) == 1){$month = "0" . $month;} # pad single digit month
		($day) = ((localtime)[3]);
		if (length($day) == 1){$day = "0" . $day;} # pad single digit day
		($hour) = ((localtime)[2]);
		if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
		($min) = ((localtime)[1]);
		if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
		($sec) = ((localtime)[0]);
		if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
		# ************************************************************************
		return $year, $month, $day, $hour, $min, $sec;
	}

sub send_alert
	{
		foreach $SendTo (@notify)
			{
				$msg = $_[0];
				$subject = $_[1];

				$smtp = Net::SMTP->new($mailServer); 	# connect to an SMTP server
				$smtp->mail( 'XTRNTUPLD_PERL@usatoday.com' );     		# use the sender's address here
				$smtp->to($SendTo);        				# recipient's address

				$smtp->data();                      			# Start the mail

				# Send the header.
				$smtp->datasend("To: $SendTo\n");
				$smtp->datasend("From: moc-wn1048\n");
				$smtp->datasend("Subject: $subject\n");
				$smtp->datasend("\n");

				# Send the body.
				$smtp->datasend($msg);
				$smtp->dataend();                   # Finish sending the mail
				$smtp->quit;                        # Close the SMTP
				

				print LOG_FILE $timestamp . " -- email/page sent to: $SendTo\n";
			}	
	}
