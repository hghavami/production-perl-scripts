#################################################################################
#
# TAXTAB.PL
#
# Created 12/2000
#
#
# updated 8/6/2002 Scott Seller Added code to check for a zero length file.
#                               Added send_alert subroutine.
# updated 05/22/2007 Houman Ghavami - Added an error email counter.  Only send 5 email errors
#								Successful exectution requires notify.txt be in the same directory as the script
#
#################################################################################
use Win32::ODBC;
use Net::SMTP;
use File::stat;

my($db) = new Win32::ODBC("dsn=esub; uid=usatSPODBC; pwd=subscribe");

$db->SetConnectOption('SQL_AUTOCOMMIT', 'SQL_AUOTCOMMIT_OFF');

$mailServer = "smtp.v4.gmti.gbahn.net";

$ACTIVE = "N";
$file_size = 0;
$ErrorCounter = 0;
$timestamp = localtime;
&CreateTimeStamp();

$LogFileName = "taxtab_" . $year . $month . $day . ".log";


open (LOG_FILE, ">>L:\\USAT\\processed\\logs\\$LogFileName") or die;

### Create email/page list

if (open(NL, "L:\\USAT\\scripts\\notify_list.txt"))
	{
		@notify = (<NL>);
		close NL;
	}
else
	{
		print "Error opening notify_list.txt\n";
		print LOG_FILE "$timestamp -- Error opening notify_list.txt\n";
		die;
	}	

### Check to make sure we didn't get an empty file

$file_size = stat("L:\\USAT\\FTP\\incoming\\TAXTAB")->size;
if ($file_size == 0)
	{
		print LOG_FILE "$timestamp -- TAXTAB is empty.\n";
		&send_alert("$timestamp -- TAXTAB is empty.\n", "TAXTAB Empty from 400");
		rename ("L:\\USAT\\FTP\\incoming\\TAXTAB", "L:\\USAT\\processed\\TAXTAB");
		die;
	}
	

open (EXTRACT_FILE, "L:\\USAT\\FTP\\incoming\\TAXTAB");
$Start_time = localtime;
print LOG_FILE "Started processing TAXTAB at $Start_time\n";

while (my $current_line = <EXTRACT_FILE>)
	{
		$current_line =~ s/\'/\`/g;
		$current_line =~ s/\,/ /g;
		$current_line =~ s/\"/\`\`/g;
		$current_line =~ s/\x00/\x20/g;
		chomp($current_line);
		$PubCode = substr($current_line,0,2);
		$FromZip = substr($current_line,2,9);
		$ToZip = substr($current_line,11,9);
		$DeliveryTypeCode = substr($current_line,20,2);
		$TaxRate1= substr($current_line,22,7);
		$TaxRate2 = substr($current_line,29,7);
		$TaxRate3 = substr($current_line,36,7);
		$TaxRate4 = substr($current_line,43,7);
		$TaxRate5 = substr($current_line,50,7);
		$TaxRateCode1 = substr($current_line,57,8);
		$TaxRateCode2 = substr($current_line,65,8);
		$TaxRateCode3 = substr($current_line,73,8);
		$TaxRateCode4 = substr($current_line,81,8);
		$TaxRateCode5 = substr($current_line,89,8);
		$PIADebitCode1= substr($current_line,97,4);
		$PIADebitCode2= substr($current_line,101,4);
		$PIADebitCode3= substr($current_line,105,4);
		$PIADebitCode4= substr($current_line,109,4);
		$PIADebitCode5= substr($current_line,113,4);
		$EffectiveDate = substr($current_line,117,7);
		$LastChangeDate = substr($current_line,124,6);
		$LastChangeTime = substr($current_line,130,6);
		$LastUser = substr($current_line,136,10);
		
		if ($db->Sql("INSERT INTO TAXTAB 
					(PubCode, FromZip, ToZip, DeliveryTypeCode, TaxRate1, TaxRate2, TaxRate3, TaxRate4, TaxRate5, 
					TaxRateCode1, TaxRateCode2, TaxRateCode3, TaxRateCode4, TaxRateCode5, 
					PIADebitCode1, PIADebitCode2, PIADebitCode3, PIADebitCode4, PIADebitCode5, 
					EffectiveDate, LastChangeDate, LastChangeTime, LastUser, ACTIVE) 
					VALUES 
					('$PubCode', '$FromZip', '$ToZip', '$DeliveryTypeCode', '$TaxRate1', '$TaxRate2', '$TaxRate3', '$TaxRate4', '$TaxRate5', 
					'$TaxRateCode1', '$TaxRateCode2', '$TaxRateCode3', '$TaxRateCode4', '$TaxRateCode5', 
					'$PIADebitCode1', '$PIADebitCode2', '$PIADebitCode3', '$PIADebitCode4', '$PIADebitCode5', 
					'$EffectiveDate', '$LastChangeDate', '$LastChangeTime', '$LastUser', '$ACTIVE')"))
			{
			#$db->Transact('SQL_ROLLBACK');
			my($err) = $db->Error;
			warn "Sql() ERROR\n";
			warn "\t\$stmt: INSERT INTO TAXTAB...\n";
			warn "\t\$err: $err\n";
			# send an email
			
			$eMsg = "INSERT INTO TAXTAB 
					(PubCode, FromZip, ToZip, DeliveryTypeCode, TaxRate1, TaxRate2, TaxRate3, TaxRate4, TaxRate5, 
					TaxRateCode1, TaxRateCode2, TaxRateCode3, TaxRateCode4, TaxRateCode5, 
					PIADebitCode1, PIADebitCode2, PIADebitCode3, PIADebitCode4, PIADebitCode5, 
					EffectiveDate, LastChangeDate, LastChangeTime, LastUser, ACTIVE) 
					VALUES 
					('$PubCode', '$FromZip', '$ToZip', '$DeliveryTypeCode', '$TaxRate1', '$TaxRate2', '$TaxRate3', '$TaxRate4', '$TaxRate5', 
					'$TaxRateCode1', '$TaxRateCode2', '$TaxRateCode3', '$TaxRateCode4', '$TaxRateCode5', 
					'$PIADebitCode1', '$PIADebitCode2', '$PIADebitCode3', '$PIADebitCode4', '$PIADebitCode5', 
					'$EffectiveDate', '$LastChangeDate', '$LastChangeTime', '$LastUser', '$ACTIVE')";

                        # Issue up to 5 error emails
                        if ($ErrorCounter < 5) {
				&send_alert("$timestamp -- \n\nError Code: $err\n\nApplication Terminated. Manual Intervention Required.\n\nSQL: $eMsg", "INSERT INTO TAXTAB FAILED");
				$ErrorCounter++;
                        }			
			exit;
		}	

} # while (<EXTRACT_FILE>)

# $db->Transact('SQL_COMMIT');

# Remove old records
$db->Sql("DELETE FROM TAXTAB WHERE ACTIVE = 'Y'"); 
# Update newly inserted records.
$db->Sql("UPDATE TAXTAB SET ACTIVE = 'Y'"); 

$db->Close();
$end_time = localtime;
print LOG_FILE "Finished processing TAXTAB at $end_time\n";
close (EXTRACT_FILE);
close (LOG_FILE);

$timestamp = join('_', split(/:/, localtime));
# $timestamp =~ join('_', split(/ /, $timestamp));
$new_name = "TAXTAB_" . $timestamp;
rename ("L:\\USAT\\FTP\\incoming\\TAXTAB", "L:\\USAT\\processed\\$new_name");

sub send_alert
	{
		foreach $SendTo (@notify)
			{
				$msg = $_[0];
				$subject = $_[1];

				$smtp = Net::SMTP->new($mailServer); 	# connect to an SMTP server
				$smtp->mail( 'TAXTAB_PERL@usatoday.com' );     		# use the sender's address here
				$smtp->to($SendTo);        				# recipient's address

				$smtp->data();                      			# Start the mail

				# Send the header.
				$smtp->datasend("To: $SendTo\n");
				$smtp->datasend("From: moc-wn1047\n");
				$smtp->datasend("Subject: $subject\n");
				$smtp->datasend("\n");

				# Send the body.
				$smtp->datasend($msg);
				$smtp->dataend();                   # Finish sending the mail
				$smtp->quit;                        # Close the SMTP
				

				print LOG_FILE $timestamp . " -- email/page sent to: $SendTo\n";
			}	
	}


sub CreateTimeStamp
	{
		# ***********************************************************************
		# Get information to produce the name of the files
		($year)=((localtime)[5]) + 1900;
		($month) = ((localtime)[4]) + 1;
		if (length($month) == 1){$month = "0" . $month;} # pad single digit month
		($day) = ((localtime)[3]);
		if (length($day) == 1){$day = "0" . $day;} # pad single digit day
		($hour) = ((localtime)[2]);
		if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
		($min) = ((localtime)[1]);
		if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
		($sec) = ((localtime)[0]);
		if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
		# ************************************************************************
		return $year, $month, $day, $hour, $min, $sec;
	}
