# #########################################################################
# 
# Filename: xtrntemail_upload.pl
# 
# Created:  2002
# 
# Author: Scott Seller, USA TODAY
# 
# This script runs at 3am every night. It extracts all data from 
# XTRNTEMAIL and sends it to the AS/400.
# 
# Updated 7 August 2002 Scott Seller changed logging to create daily log files
# updated 11 September 2002 Scott Seller changed number of characters sent to 400
#               for serial number from 5 to 11.
# Update 21 March 2003 - Andy East - Added an archive file. Added logic to generate the
#                                   archive file and then copy it into the outgoing folder
# updated 09/24/2003 Houman Ghavami - Changed length of emailAddress for the PTI/SBSTRNAA project
# Updated 05/19/2004 Andy East - Add logic to encrypt data and FTP to public Enterprise FTP Server
# Updated 07/14/2011 Houman Ghavami - Replaced FTP with SFTP.
# #########################################################################

use Win32::ODBC;
use Net::SMTP;
use File::Copy;
use Net::FTP;
use Win32::Process;
use File::Util;
use Date::Format;
use chilkat;

# Configure following based on prod/test environment
my $debug = "ON";


my $server = "ftp.usat.com";  		# ftp server
my $sserver = "ftps.gannett.com";	# sftp server
my $todir = "incoming";  			# destination server directory to put file
my $stodir = "/incoming/";
my $sport = "22";					# sftp port

# USE THIS FOR PROD
my $login = "extranet";   			# ftp login
my $slogin = "usat-webportal";		# sftp login
my $pass = "20extra\@net04";    	# ftp password
my $spass = "EMkXp6MGCniVYeI870Wl";	# sftp password

# USE THIS FOR TEST
#my $login = "extftptest";   		# ftp login
#my $pass = "19extftptest99";   	# ftp password

## Alternate DROP Site
#my $login = "circftp";    			# ftp login alternate drop site
#my $pass = "19circftp99";    		# ftp password alternate drop site
#my $server = "10.204.2.57";    	# alternate drop site


$mailServer = "smtp.v4.gmti.gbahn.net";

my $rootDir = "L:\\USAT\\";
my $logRootDir = "L:\\USAT\\";
my $workingDir = $rootDir . "processed\\archive\\";
my $homedir = "C:\\gnupg";  #location of the GNU keystore

my $encryptedfile = "";  		#holder vaiable for name of encrypted file
my $filetoencrypt = "";  		#holder variable for source file to encrypt
my $fileToSend = "";    		#holder variable for the current file to ftp
my $archive_filename_full = ""; 	#holder variable for full archived file name

my($db) = new Win32::ODBC("dsn=esub; uid=usatSPODBC; pwd=subscribe");

$db->SetConnectOption('SQL_AUTOCOMMIT', 'SQL_AUOTCOMMIT_OFF');

&CreateTimeStamp();
$Start_time = localtime;
$numb_rows = 0;
$LogFileTime = $year . $month . $day;
$LogFileName = "XTRNTEMAIL_UPLOAD_" . $LogFileTime . ".log";
$archive_filename = "XTRNTEMAIL_UPLOAD" . "\.$month$day$hour$sec";
my $localextract_file_name = "XTRNTEMAIL_UPLOAD";
my $slocalextract_file_name = "XTRNTEMAIL_UPLOAD.gpg";
#$encryptedfile = $workingDir . $archive_filename . ".gpg";
$archive_filename_full = $workingDir . $archive_filename . ".gpg";

open (LOG_FILE, ">>" . $logRootDir . "processed\\logs\\$LogFileName");

#open (EXTRACT_FILE_ARCHIVE, ">$workingDir$archive_filename");
open (EXTRACT_FILE_ARCHIVE, ">$workingDir$localextract_file_name");

print LOG_FILE "\n\n $Start_time -- Started creating XTRNTEMAIL_UPLOAD\n";

### Create email/page list

if (open(NL, "L:\\USAT\\scripts\\notify_list.txt"))
	{
		@notify = (<NL>);
		close NL;
	}
else
	{
		print "Error opening notify_list.txt\n";
		print LOG_FILE "$timestamp -- Error opening notify_list.txt\n";
		die;
}	
	
$db->Sql("SELECT * FROM XTRNTEMAIL");

while ($db->FetchRow())
	{
		my(%data) = $db->DataHash();

		$PubCode = sprintf("%-2.2s",$data{'PubCode'});
		# $AccountNum = sprintf("%-7.7s",$data{'AccountNum'});
		$AccountNum = sprintf("%-7.7s",substr($data{'AccountNum'},-7));
		$WebID = sprintf("%-20.20s",$data{'WebID'});
		$EmailAddress = sprintf("%-60.60s",$data{'EmailAddress'});
		$WebPassword= sprintf("%-10.10s",$data{'WebPassword'});
		$AgencyName = sprintf("%-4.4s",$data{'AgencyName'});
		$TransType = sprintf("%-1.1s",$data{'TransType'});
		$EmailSent = sprintf("%-1.1s",$data{'EmailSent'});
		$PermStartDate = sprintf("%-8.8s",$data{'PermStartDate'});
		$SerialNum = sprintf("%-11.11s",$data{'SerialNum'});
		$DateUpdated = sprintf("%-8.8s",$data{'DateUpdated'});
		$TimeUpdated = sprintf("%-6.6s",$data{'TimeUpdated'});
		$GiftTMCCode = sprintf("%-1.1s",$data{'GiftTMCCode'});
		
		$TextString = "$PubCode" . "$AccountNum" . "$WebID" . "$EmailAddress" . "$WebPassword" . "$AgencyName" . "$TransType" . "$EmailSent" . "$PermStartDate" . "$SerialNum" . "$DateUpdated" . "$TimeUpdated" . "$GiftTMCCode" . "\n";
		
		print EXTRACT_FILE_ARCHIVE $TextString;
		$numb_rows++;
#		print EXTRACT_FILE_ARCHIVE $db->Error();

} # while ($db->FetchRow())

$end_time = localtime;
print LOG_FILE "$end_time -- Finished generating XTRNTEMAIL_UPLOAD. $numb_rows records transfered.\n";

close (EXTRACT_FILE_ARCHIVE);

$db->Close();

# encrypt transaction file
$encryptedfile = $workingDir . $localextract_file_name . ".gpg";
$filetoencrypt = $workingDir . $localextract_file_name;
encryptStuff();
$end_time = localtime;

# if encrypted file exists
if (-e $encryptedfile)
{ 
	print LOG_FILE "Finished encrypting $filetoencrypt at $end_time\n";
}
else {
	print LOG_FILE "Failed to encrypt $filetoencrypt at $end_time\n";
	&send_alert("$end_time -- FAILED To Encrypt Email file - $filetoencrypt\n\n  MANUAL SEND REQUIRED !", "GNUPGP Encrypt Failed - EMail File");
	print LOG_FILE "Alert Sent. Program Terminating.\n\n";
	die;
}

#make a temporary copy of the encrypted file for ftping to maintain old naming scheme.
#copy( "$encryptedfile", $workingDir . "XTRNTEMAIL_UPLOAD.gpg") || die "cannot copy the email data file";
copy( "$encryptedfile", $archive_filename_full) || die "cannot copy the email data file";
# rename XTRNTEMAIL_UPLOAD TO XTRNTEMAIL_UPLOADMMDDHHSS
rename( $filetoencrypt, $workingDir . $archive_filename) || die "cannot rename to archive email file";
print LOG_FILE "Made a temporary copy of the encrypted file for ftping to server\n";

# ftp file
$fileToSend = $workingDir . $slocalextract_file_name;
#sendftp();
send_sFTP();

$end_time = localtime;
print LOG_FILE "Finished ftping $fileToSend at $end_time\n";

#delete unencrypted local archive copy and temporary files
#@filelist = ("$workingDir$archive_filename", $workingDir . "XTRNTEMAIL_UPLOAD.gpg");
#unlink @filelist;
$end_time = localtime;
print LOG_FILE "Finished deleting temporary files at $end_time\nExitting.";

close (LOG_FILE);

#Exit cleanly
exit;

sub CreateTimeStamp
	{
		# Get information to produce the name of the files
		($year)=((localtime)[5]) + 1900;
		($month) = ((localtime)[4]) + 1;
		if (length($month) == 1){$month = "0" . $month;} # pad single digit month
		($day) = ((localtime)[3]);
		if (length($day) == 1){$day = "0" . $day;} # pad single digit day
		($hour) = ((localtime)[2]);
		if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
		($min) = ((localtime)[1]);
		if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
		($sec) = ((localtime)[0]);
		if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
		return $year, $month, $day, $hour, $min, $sec;
	}
	
sub encryptStuff {
	system "del $encryptedfile";
	system "gpg --homedir $homedir --output $encryptedfile --recipient hghavami\@usatoday.com --encrypt $filetoencrypt";
}

sub sendftp {
	$filetypeswitch= "binary";
	#set as default

	my $ftp;
	
	if (my($ftp) = Net::FTP->new("$server",Passive => 1)) {
	   if($ftp) {
		$ftp->debug("$debug");
		$ftp->login("$login","$pass") or
			die "Failed to log into server $server";
			#or 
			#	($ftpstep = $bad and $msg = "$fail_login"."$login"."/"."$pass") if $ftpstep eq $good;
		if ($filetypeswitch eq "ascii") {
				$ftp->ascii();
		} else {
				$ftp->binary();	
		}
		
		if ($todir eq "root") {
			#no action required
			allmessages("Put file to $todir directory \n");
		} else {
			allmessages("Changing to directory $todir \n");
			$ftp->cwd("$todir");
		}
		
		$ftp->put("$fileToSend");
			# if $ftpstep eq $good;

		$ftp->quit();

   		$msg = "FTP over the WAN/VPN from $fileToSend  to $server as $filetypeswitch successful";
		allmessages($msg."\n");

	   } else {
		
		$processed = $bad and $ftpstep = $bad and $msg = "$fail_connect"."$server";
		allmessages($msg."\n");	
	   }
	
 	} else {

       	$processed = $bad and $ftpstep = $bad and $msg = "$fail_connect"."$server";
		allmessages($msg."\n");	
	}
}

sub allmessages {
	print  LOG_FILE "$_[0]";
}


#*********************************************************************
#** SUBROUTINE THAT SENDS A FILE VIA SFTP.
#*********************************************************************
sub send_sFTP
{

  my $system   = $sserver;
  my $login    = $slogin;
  my $password = $spass;
  $todir = $stodir;    # directory on remote server
  my $filename = $fileToSend;
  my $InputDir = $workingDir;

  my $port = $sport;

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;

$sftp = new chilkat::CkSFtp();


  # The filename is passed with the full path.
  #
  my($FileUtilHandle) = File::Util->new();
  my $short_filename = $FileUtilHandle->strip_path($filename);

  #  Any string automatically begins a fully-functional 30-day trial.
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " connected to $system\n";
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.  This example uses password authenication.
  $success = $sftp->AuthenticatePw( "$login", "$password" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system\n";
    $ErrorMsg = &now . " Could not log on to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " logged on to $system\n";
  }

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Initialized SFTP subsystem\n";
  }

  #  To find the full path of our user account's home directory,
  #  call RealPath like this:

  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  #  Open a file for writing on the SSH server.
  #  If the file already exists, it is overwritten.
  #  (Specify "createNew" instead of "openOrCreate" to
  #  prevent overwriting existing files.)

  print "remote file: $absPath" . "/" . "$todir$short_filename\n";
  # print "remote file: $absPath" . "/" . "$todir$filename\n";
  $handle = $sftp->openFile( "$absPath" . "/" . "$todir$short_filename", "writeOnly", "openOrCreate" );
  # $handle = $sftp->openFile( "$absPath" . "/" . "$todir$filename", "writeOnly", "openOrCreate" );

  # if ( $handle eq null )
  if ( $handle eq "" )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not open file in remote system\n";
    $ErrorMsg = &now . " Could not open file in remote system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Opened remote file: $absPath$todir$filename\n";
  }

  #  Upload from the local file to the SSH server.
  $success = $sftp->UploadFile( $handle, "$filename" );
  # $success = $sftp->UploadFile( $handle, "$InputDir$filename" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not upload file to remote system\n";
    $ErrorMsg = &now . " Could not upload file to remote system\n";
    return "0|$ErrorMsg";
  }

  #  Close the file.
  $success = $sftp->CloseHandle($handle);
  if ( $success != 1 )
  {
    print "closehandle: " . $sftp->lastErrorText() . "\n";
    exit;
  }

  print "Success." . "\n";

} # sub send_sFTP


#*********************************************************************
#** SUBROUTINE THAT PRINTS THE TIME.
#*********************************************************************
#
sub now
	{
		time2str("%T ", time)
	}

sub send_alert
	{
		foreach $SendTo (@notify)
			{
				$msg = $_[0];
				$subject = $_[1];

				$smtp = Net::SMTP->new($mailServer); 	# connect to an SMTP server
				$smtp->mail( 'XTRNTEMAIL_UPLOAD_PERL@usatoday.com' );     		# use the sender's address here
				$smtp->to($SendTo);        				# recipient's address

				$smtp->data();                      			# Start the mail

				# Send the header.
				$smtp->datasend("To: $SendTo\n");
				$smtp->datasend("From: moc-wn1048\n");
				$smtp->datasend("Subject: $subject\n");
				$smtp->datasend("\n");

				# Send the body.
				$smtp->datasend($msg);
				$smtp->dataend();                   # Finish sending the mail
				$smtp->quit;                        # Close the SMTP
				

				print LOG_FILE $timestamp . " -- email/page sent to: $SendTo\n";
			}	
	}

