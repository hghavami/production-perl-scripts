use strict;
use File::stat;
use File::util;
use chilkat;
use Attempt;

my $msg;
my $ErrorMsg;

my $gpgHomeDir = "c:\\gnupg";

# &decrypt_file ($query_fileName, $query_keyFile, $query_passphrase, $query_decryptedfile);

#*********************************************************************
#** DECRYPTS FILES
#*********************************************************************
#
sub decrypt_file
	{

		my $encryptedfile = $_[0];
		my $key_file = $_[1];
		my $passphrase = $_[2];
		my $outfile = $_[3];

		# workaround for file::stat not working
		(my $dev,my $ino,my $mode,my $nlink,my $uid,my $gid,my $rdev,my $size,my $atime,my $mtime,my $ctime,my $blksize,my $blocks) = stat($encryptedfile);

		$msg = "DECRYPTDATA -  encrypted filename is $encryptedfile \n";
		allmessages($msg);
		# $msg = "DECRYPTDATA -  encrypted filesize is " . stat($encryptedfile)->size . "\n";
		$msg = "DECRYPTDATA -  encrypted filesize is " . $size . "\n";
		allmessages($msg);
		$msg = "DECRYPTDATA -  BEGINNING DECRYPTION \n";
		allmessages($msg);

		if ($passphrase eq "" or $passphrase eq "NONE")
			{
				# print "1 system gpg --homedir $gpgHomeDir -v --yes -r $key_file --outfile $outfile -d $encryptedfile\n";
				system "gpg --homedir $gpgHomeDir -v --yes --output \"$outfile\" -d \"$encryptedfile\"";
			}
		else
			{
				# print "system gpg --homedir $gpgHomeDir -v --yes --passphrase $passphrase -r $key_file --outfile $outfile -d $encryptedfile";
				system "gpg --homedir $gpgHomeDir -v --yes --passphrase $passphrase -r $key_file --output \"$outfile\" -d \"$encryptedfile\"";
			}

		if (-e $outfile)
			{
				# workaround for file::stat not working
				(my $dev,my $ino,my $mode,my $nlink,my $uid,my $gid,my $rdev,my $size,my $atime,my $mtime,my $ctime,my $blksize,my $blocks) = stat($outfile);

				$msg = "DECRYPTDATA -  decrypted filename is $outfile \n";
				allmessages($msg);
				# $msg = "DECRYPTDATA -  decrypted filesize is " . stat($outfile)->size ."\n";
				$msg = "DECRYPTDATA -  decrypted filesize is " . $size ."\n";
				allmessages($msg);
				$msg = "DECRYPTDATA -  END OF DECRYPTION \n";
				allmessages($msg);
				return $outfile
			}
		else
			{
				print LOG_FILE &now, " Unable to decrypt file.\n";
			}

	} # end decrypt_file

sub allmessages {
		print "$_[0]";
		print LOG_FILE &now, " $_[0]";
}

#*********************************************************************
#** CHECKS IF A FILE IS STABLE.
#*********************************************************************
#
sub stable
	{
		my $stable = 0;
		my $fileToTest = $_[0];

		# Instantiate a new File::Util object
		my($f) = File::Util->new();


		my $size = $f->size($fileToTest);
		sleep 5;
		my $size2 = $f->size($fileToTest);
		if ($size == $size2)
			{
				$stable = 1;
			}
			else
			{
				$stable = 0;
			}

		return $stable;

} # sub stable


#*********************************************************************
#** SUBROUTINE THAT PRINTS THE TIME.
#*********************************************************************
#
sub now
	{
		time2str("%T ", time)
	}

return 1;

#*********************************************************************
#** SUBROUTINE THAT SENDS A FILE VIA FTP.
#*********************************************************************
#
sub SendFtp
	{
		my $system = $_[0];
		my $login = $_[1];
		my $password = $_[2];
		my $todir = $_[3];
		my $filename = $_[6];
		my $InputDir = $_[7];

		print "$system, $login, $password\n";

		my($FileUtilHandle) = File::Util->new();
		my $size = $FileUtilHandle->size($filename);


		# ===========================================================================
		# Instantiate Net::FTP and create an ftp object
		# ===========================================================================
		## passive - If set to a non-zero value then all data transfers will be done using passive mode.
		## passive - If set to zero then data transfers will be done using active mode.
		## 04232008 - SS - Passive set to 1.
		my $ftpserver = Net::FTP->new("$system", Debug => 1, Passive => 1);
		# my $ftpserver = Net::FTP->new("$system", Debug => 1);
		if ($ftpserver)
			{
				print LOG_FILE &now, " Opened connection to $system\n";
				if ($ftpserver->login($login,$password))
					{
						print LOG_FILE &now, " Logged in to $system as $login\n";
						print $todir;
						if ($todir eq "root")
							{
								$ftpserver->binary;
								if ($ftpserver->put("$InputDir$filename"))
									{
										print LOG_FILE &now, " Successfully transferred $filename\n";
									}
								else
									{
										print LOG_FILE &now, " !!! Unable to transfer $filename\n";
										$ErrorMsg = &now . " Unable to transfer $filename\n";
										# return 0;
										return "0|$ErrorMsg";
									} #($ftpserver->put("$filename")
							}
						else
							{
									if ($ftpserver->cwd("$todir"))
										{
											print LOG_FILE &now, " Changed directory to $todir\n";
											$ftpserver->binary;
											if ($ftpserver->put("$InputDir$filename"))
												{
													print LOG_FILE &now, " Successfully transferred $filename\n";
													return "1|Successful\n";
												}
											else
												{
													print LOG_FILE &now, " !!! Unable to transfer $filename\n";
													$ErrorMsg = &now . " Unable to transfer $filename\n";
													# return 0;
													return "0|$ErrorMsg";
												} #($ftpserver->put("$filename")
										}
									else
										{
											print LOG_FILE &now, " !!! Unable to change directory to $todir\n";
											$ErrorMsg = &now . " Unable to change directory to $todir\n";
											# return 0;
											return "0|$ErrorMsg";
										} #if ($ftpserver->cwd("$todir"))
							} #if ($todir == "root")
					}
				else
					{
						print LOG_FILE &now, " !!! Could not login to $system\n";
						$ErrorMsg = &now . " Could not login to $system\n";
						# return 0;
						return "0|$ErrorMsg";
					} #($ftp->login)
			}
		else
			{
				print LOG_FILE &now, " !!! Could not open FTP connection to $system\n";
				$ErrorMsg = &now . " Could not open FTP connection to $system\n";
				# return 0;
				return "0|$ErrorMsg";
			} #($ftpserver)

	}#sub send_ftp

#*********************************************************************
#** SUBROUTINE THAT ENCRYPTS A FILE.
#*********************************************************************
#
sub encrypt_file #($filename, $key_name, $outputfile)
	{
		my $filename = $_[0];
		my $key_name = $_[1];
		### my $outfile = $_[2];

		system "gpg --homedir $gpgHomeDir --logger-fd 1 --batch --recipient $key_name --always-trust --yes -v --encrypt-files $filename";
		# system "gpg --homedir $gpgHomeDir --logger-fd 1 --batch --armor --recipient $key_name --always-trust --yes -v --encrypt-files $filename ";


	} # end encrypt_file

#*********************************************************************
#** SUBROUTINE THAT ZIPS A FILE.
#*********************************************************************
#
sub zipfile
	{
		my $action = $_[0];
		my $filename = $_[1];
		my $Output = $_[2];
		my $password = $_[3];
		if ($password eq "" or $password eq "NONE")
			{
				if ($action =~ /u/i)
					{
						print LOG_FILE &now, " File $filename needs to be unzipped.\n";
						system "wzunzip $filename $Output";
					}
				elsif ($action =~ /z/i)
					{
						print LOG_FILE &now, " File $filename needs to be zipped.\n";
						system "wzzip $Output $filename";
					}
			}
		else
			{
				print "using a password: $password\n";
				if ($action =~ /u/i)
					{
						print LOG_FILE &now, " File $filename needs to be unzipped.\n";
						system "wzunzip -s$password $filename $Output";
					}
				elsif ($action =~ /z/i)
					{
						print LOG_FILE &now, " File $filename needs to be zipped.\n";
						system "wzzip -s$password $Output $filename";
					}
			} #($password ne "")

	} #zipfile


#*********************************************************************
#** SUBROUTINE THAT SENDS A FILE VIA FTP.
#*********************************************************************
#

sub GetFtpFilelist
{
  my $system       = $_[0];
  my $login        = $_[1];
  my $password     = $_[2];
  my $todir        = $_[3];
  my $FileSpec     = $_[4];
  my $FileArrayRef = $_[5];

  # my($FileUtilHandle) = File::Util->new();
  # 	my $size = $FileUtilHandle->size($filename);

  # ===========================================================================
  # Instantiate Net::FTP and create an ftp object
  # ===========================================================================
  ## passive - If set to a non-zero value then all data transfers will be done using passive mode.
  ## passive - If set to zero then data transfers will be done using active mode.
  ## 04232008 - SS - Passive set to 1.
  my $ftpserver = Net::FTP->new( "$system", Debug => 1, Passive => 0, Timeout=> 240 );

  # my $ftpserver = Net::FTP->new("$system", Debug => 1);
  if ($ftpserver)
  {
    $ftpserver->binary;
    print LOG_FILE &now, " Opened connection to $system\n";
    if ( $ftpserver->login( $login, $password ) )
    {
      print LOG_FILE &now, " Logged in to $system as $login\n";

      # print $todir;
      if ( $todir eq "root" )
      {
        @$FileArrayRef = $ftpserver->ls($FileSpec);
        if ( $#$FileArrayRef >= 0 )
        {
          print LOG_FILE &now, " Retrieved filelist ($FileSpec)\n";
          return 1;
        }
        else
        {
          print LOG_FILE &now, " !!! Unable to retrieve filelist ($FileSpec)\n";
          $ErrorMsg = &now . " Unable to retrieve filelist ($FileSpec)\n";
          return "0|$ErrorMsg";
        }
      }
      else
      {
        if ( $ftpserver->cwd("$todir") )
        {
          print LOG_FILE &now, " Changed directory to $todir\n";

          # @$FileArrayRef = $ftpserver->ls($FileSpec);
          my @FileList = $ftpserver->ls($FileSpec);

          # if ($#$FileArrayRef >= 0)
          if ( $#FileList >= 0 )
          {
            foreach my $FileName (@FileList)
            {
              # print LOG_FILE "test: $FileName\n";
              my $FileSize = $ftpserver->size($FileName);
              # print LOG_FILE "test: $FileSize\n";
              my $FileDate = $ftpserver->mdtm($FileName);
              push( @$FileArrayRef, $FileName . "|" . $FileSize . "|" . $FileDate );
            }
            print LOG_FILE &now, " Retrieved filelist ($FileSpec)\n";
            return 1;
          }
          else
          {
            print LOG_FILE &now, " !!! Unable to retrieve filelist ($FileSpec)\n";
            $ErrorMsg = &now . " Unable to retrieve filelist or no files found ($FileSpec)\n";
            return "0|$ErrorMsg";
          }
        }
        else
        {
          print LOG_FILE &now, " !!! Unable to change directory to $todir\n";
          $ErrorMsg = &now . " Unable to change directory to $todir\n";
          return "0|$ErrorMsg";
        }    #if ($ftpserver->cwd("$todir"))
      }    #if ($todir == "root")
    }
    else
    {
      print LOG_FILE &now, " !!! Could not login to $system\n";
      $ErrorMsg = &now . " Could not login to $system\n";

      # return 0;
      return "0|$ErrorMsg";
    }    #($ftp->login)
  }
  else
  {
    print LOG_FILE &now, " !!! Could not open FTP connection to $system\n";
    $ErrorMsg = &now . " Could not open FTP connection to $system\n";

    # return 0;
    return "0|$ErrorMsg";
  }    #($ftpserver)

}    #sub GetFtpFilelist



#*********************************************************************
#** SUBROUTINE THAT GETS A FILE VIA FTP.
#*********************************************************************
#
sub GetFtpFile
{

  # &GetFtpFile("ftp.usat.com", "elias2", "20elias205", "\incoming", $FileSpec, $OutputFileSpec);

  my ($FileUtilHandle) = File::Util->new();

  my $system         = $_[0];
  my $login          = $_[1];
  my $password       = $_[2];
  my $todir          = $_[3];
  my $FileSpec       = $_[4];
  my $OutputFileSpec = $_[5];

  # ===========================================================================
  # Instantiate Net::FTP and create an ftp object
  # ===========================================================================
  ## passive - If set to a non-zero value then all data transfers will be done using passive mode.
  ## passive - If set to zero then data transfers will be done using active mode.
  ## 04232008 - SS - Passive set to 1.
  my $ftpserver = Net::FTP->new( "$system", Debug => 1, Passive => 1, Timeout=> 240 );

  # my $ftpserver = Net::FTP->new("$system", Debug => 1);
  if ($ftpserver)
  {
    print LOG_FILE &now, " Opened connection to $system\n";
    if ( $ftpserver->login( $login, $password ) )
    {
      print LOG_FILE &now, " Logged in to $system as $login\n";

      # print $todir;
      if ( $todir eq "root" )
      {
        $ftpserver->binary;
        if ( $ftpserver->get( "$FileSpec", "$OutputFileSpec" ) )
        {
          my $RemoteFileSize = $ftpserver->size("$FileSpec");
          my $LocalFileSize  = $FileUtilHandle->size("$OutputFileSpec");
          print "$RemoteFileSize : $LocalFileSize\n";
          if ( $RemoteFileSize == $LocalFileSize )
          {
            print LOG_FILE &now, " Successfully transferred $FileSpec\n";

            # delete the file and the associated .meta file
            my $MetaFile = "$FileSpec" . ".meta";
            if ( $ftpserver->delete("$MetaFile") )
            {
              print LOG_FILE &now, " Deleted $MetaFile\n";
            }
            if ( $ftpserver->delete("$FileSpec") )
            {
              print LOG_FILE &now, " Deleted $FileSpec\n";
            }
            return "1|Successful\n";
          }
          else
          {
            print LOG_FILE &now, " File size does not match. Local file:$LocalFileSize, Remote:$RemoteFileSize\n";
            unlink $FileSpec;
            $ErrorMsg = &now . " File size does not match. Transfer of $FileSpec failed\n";
            return "0|$ErrorMsg";
          }
        }
        else
        {
          print LOG_FILE &now, " !!! Unable to transfer $FileSpec\n";
          $ErrorMsg = &now . " Unable to transfer $FileSpec\n";
          return "0|$ErrorMsg";
        }    #($ftpserver->put("$filename")
      }
      else
      {
        if ( $ftpserver->cwd("$todir") )
        {
          print LOG_FILE &now, " Changed directory to $todir\n";
          $ftpserver->binary;
          if ( $ftpserver->get( "$FileSpec", "$OutputFileSpec" ) )
          {
            my $RemoteFileSize = $ftpserver->size("$FileSpec");
            my $LocalFileSize  = $FileUtilHandle->size("$OutputFileSpec");
            print "$RemoteFileSize : $LocalFileSize\n";
            if ( $RemoteFileSize == $LocalFileSize )
            {
              print LOG_FILE &now, " Successfully transferred $FileSpec\n";

              # delete the file and the associated .meta file
              my $MetaFile = "$FileSpec" . ".meta";
              if ( $ftpserver->delete("$MetaFile") )
              {
                print LOG_FILE &now, " Deleted $MetaFile\n";
              }
              if ( $ftpserver->delete("$FileSpec") )
              {
                print LOG_FILE &now, " Deleted $FileSpec\n";
              }
              return "1|Successful\n";
            }
            else
            {
              print LOG_FILE &now, " File size does not match. Local file:$LocalFileSize, Remote:$RemoteFileSize\n";
              unlink $FileSpec;
              $ErrorMsg = &now . " File size does not match. Transfer of $FileSpec failed\n";
              return "0|$ErrorMsg";
            }
          }
          else
          {
            print LOG_FILE &now, " !!! Unable to transfer $FileSpec\n";
            $ErrorMsg = &now . " Unable to transfer $FileSpec\n";

            # return 0;
            return "0|$ErrorMsg";
          }    #($ftpserver->put("$filename")
        }
        else
        {
          print LOG_FILE &now, " !!! Unable to change directory to $todir\n";
          $ErrorMsg = &now . " Unable to change directory to $todir\n";

          # return 0;
          return "0|$ErrorMsg";
        }    #if ($ftpserver->cwd("$todir"))
      }    #if ($todir == "root")
    }
    else
    {
      print LOG_FILE &now, " !!! Could not login to $system\n";
      $ErrorMsg = &now . " Could not login to $system\n";

      # return 0;
      return "0|$ErrorMsg";
    }    #($ftp->login)
  }
  else
  {
    print LOG_FILE &now, " !!! Could not open FTP connection to $system\n";
    $ErrorMsg = &now . " Could not open FTP connection to $system\n";

    # return 0;
    return "0|$ErrorMsg";
  }    #($ftpserver)

}    #sub send_ftp


#*********************************************************************
#** SUBROUTINE THAT GETS A FILE VIA FTP.
#*********************************************************************
#
sub GetFtpFileSize
  {

    # &GetFtpFile("ftp.usat.com", "elias2", "20elias205", "\incoming", $FileSpec);

    my $system   = $_[0];
    my $login    = $_[1];
    my $password = $_[2];
    my $todir    = $_[3];
    my $FileSpec = $_[4];

    print "$FileSpec\n";

    # ===========================================================================
    # Instantiate Net::FTP and create an ftp object
    # ===========================================================================
    ## passive - If set to a non-zero value then all data transfers will be done using passive mode.
    ## passive - If set to zero then data transfers will be done using active mode.
    ## 04232008 - SS - Passive set to 1.
    my $ftpserver = Net::FTP->new( "$system", Debug => 1, Passive => 1, Timeout=> 240 );

    # my $ftpserver = Net::FTP->new("$system", Debug => 1);
    if ($ftpserver)
    {
      print LOG_FILE &now, " Opened connection to $system\n";
      if ( $ftpserver->login( $login, $password ) )
      {
        print LOG_FILE &now, " Logged in to $system as $login\n";
        print $todir;
        if ( $todir eq "root" )
        {
          $ftpserver->binary;
          if ( my $FileSize = $ftpserver->size("$FileSpec") )
          {

            # print LOG_FILE &now, " Successfully transferred $FileSpec\n";
            return $FileSize;
          }
          else
          {
            print LOG_FILE &now, " !!! Unable to get file size of $FileSpec\n";
            $ErrorMsg = &now . " Unable to get file size of $FileSpec\n";

            # return 0;
            return "0|$ErrorMsg";
          }    #($ftpserver->put("$filename")
        }
        else
        {
          if ( $ftpserver->cwd("$todir") )
          {
            print LOG_FILE &now, " Changed directory to $todir\n";
            $ftpserver->binary;
            if ( my $FileSize = $ftpserver->size("$FileSpec") )
            {

              # print LOG_FILE &now, " Successfully transferred $FileSpec\n";
              return $FileSize;
            }
            else
            {
              print LOG_FILE &now, " !!! Unable to get file size of $FileSpec\n";
              $ErrorMsg = &now . " Unable to get file size of $FileSpec\n";

              # return 0;
              return "0|$ErrorMsg";
            }    #($ftpserver->put("$filename")
          }
          else
          {
            print LOG_FILE &now, " !!! Unable to change directory to $todir\n";
            $ErrorMsg = &now . " Unable to change directory to $todir\n";

            # return 0;
            return "0|$ErrorMsg";
          }    #if ($ftpserver->cwd("$todir"))
        }    #if ($todir == "root")
      }
      else
      {
        print LOG_FILE &now, " !!! Could not login to $system\n";
        $ErrorMsg = &now . " Could not login to $system\n";

        # return 0;
        return "0|$ErrorMsg";
      }    #($ftp->login)
    }
    else
    {
      print LOG_FILE &now, " !!! Could not open FTP connection to $system\n";
      $ErrorMsg = &now . " Could not open FTP connection to $system\n";

      # return 0;
      return "0|$ErrorMsg";
    }    #($ftpserver)

  }    # sub GetFtpFileSize



#*********************************************************************
#**
#*********************************************************************
#
sub GetFileSize
	{
		my $FileToTest = $_[0];
		print "subroutine: $FileToTest\n";

		my($f) = File::Util->new();

		my $size = $f->size($FileToTest);

		return $size;
	}

#*********************************************************************
#**
#*********************************************************************
#
sub GetFileAge
	{

	 # returns age of file in minutes


		my $FileToTest = $_[0];
		my $right_now = time;

		print "--------> $FileToTest\n";

		(my $dev,my $ino,my $mode,my $nlink,my $uid,my $gid,my $rdev,my $size,my $atime,my $mtime,my $ctime,my $blksize,my $blocks) = stat($FileToTest);

		# my $file_mod_date = stat($FileToTest)->mtime;
		my $file_mod_date = $mtime;
		my $AgeOfFile = (($right_now - $file_mod_date)/60);

		return $AgeOfFile;

	}


#*********************************************************************
#** SUBROUTINE THAT LOADS NOTIFICATION LIST.
#*********************************************************************
sub load_notification_list
{

  my $notification_list_file = $_[0];
  my $NotifyList;

  open( NOTIFICATION_LIST, "<$notification_list_file" ) || die "Error999 - Can't open $notification_list_file";

  my $counter = 0;
  foreach my $line (<NOTIFICATION_LIST>)
  {
    my @current_line = split( /\|/, $line );
    if ( $current_line[0] eq "A" )
    {
      chomp( $current_line[1] );
      if ( $counter == 0 )
      {
        $NotifyList = $current_line[1];
      }
      else
      {
        $NotifyList = $NotifyList . "," . $current_line[1];
      }
      $counter++;

      # $notification_list_data{ $current_line[0] } = $current_line[1];
    }
  }

  close NOTIFICATION_LIST;
  return $NotifyList;
}    # END load_notification_list

#*********************************************************************
#** SUBROUTINE THAT SENDS A FILE VIA SFTP.
#*********************************************************************
sub send_sFTP
{

  my $system   = $_[0];
  my $login    = $_[1];
  my $password = $_[2];
  my $todir    = $_[3];    # directory on remote server
  my $filename = $_[6];
  my $InputDir = $_[7];

  my $port = 22;

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;

  $sftp = new chilkat::CkSFtp();


  # The filename is passed with the full path.
  #
  my($FileUtilHandle) = File::Util->new();
  my $short_filename = $FileUtilHandle->strip_path($filename);

  #  Any string automatically begins a fully-functional 30-day trial.
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " connected to $system\n";
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.  This example uses password authenication.
  $success = $sftp->AuthenticatePw( "$login", "$password" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system\n";
    $ErrorMsg = &now . " Could not log on to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " logged on to $system\n";
  }

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Initialized SFTP subsystem\n";
  }

  #  To find the full path of our user account's home directory,
  #  call RealPath like this:

  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  #  Open a file for writing on the SSH server.
  #  If the file already exists, it is overwritten.
  #  (Specify "createNew" instead of "openOrCreate" to
  #  prevent overwriting existing files.)

  print "remote file: $absPath" . "/" . "$todir$short_filename\n";
  # print "remote file: $absPath" . "/" . "$todir$filename\n";
  $handle = $sftp->openFile( "$absPath" . "/" . "$todir$short_filename", "writeOnly", "openOrCreate" );
  # $handle = $sftp->openFile( "$absPath" . "/" . "$todir$filename", "writeOnly", "openOrCreate" );

  # if ( $handle eq null )
  if ( $handle eq "" )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not open file in remote system\n";
    $ErrorMsg = &now . " Could not open file in remote system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Opened remote file: $absPath$todir$filename\n";
  }

  #  Upload from the local file to the SSH server.
  $success = $sftp->UploadFile( $handle, "$filename" );
  # $success = $sftp->UploadFile( $handle, "$InputDir$filename" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not upload file to remote system\n";
    $ErrorMsg = &now . " Could not upload file to remote system\n";
    return "0|$ErrorMsg";
  }

  #  Close the file.
  $success = $sftp->CloseHandle($handle);
  if ( $success != 1 )
  {
    print "closehandle: " . $sftp->lastErrorText() . "\n";
    exit;
  }

  print "Success." . "\n";

  return 1;
} # sub send_sFTP



#*********************************************************************
#** SUBROUTINE THAT GETS A DIRECTORY VIA SFTP.
#*********************************************************************
sub GetSftpFilelist
{


  my $system = $_[0];
  my $login = $_[1];
  my $password = $_[2];
  my $DirToList = $_[3];
  my $FileSpec = $_[4];
  my $FileArrayRef = $_[5];

  print "---> Files to get: $FileSpec\n";

  my $port = 22;

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;
  $sftp = new chilkat::CkSFtp();

  #**************************************************************
  # Standard code that connects and set up the ssh/sftp session
  #**************************************************************
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "connected to $system\n";
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.  This example uses password authenication.
  $success = $sftp->AuthenticatePw( "$login", "$password" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system\n";
    $ErrorMsg = &now . " Could not log on to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "logged on to $system\n";
  }

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "Initialized SFTP subsystem\n";
  }
  #**************************************************************
  # End Standard code that connects and set up the ssh/sftp session
  #**************************************************************


  $handle = $sftp->openDir($DirToList);
  if ( $handle eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }

####**************************************************************
#### New get dir list routine. returns file size and date as well as name
####**************************************************************

  #**************************************************************
  # sft does not support changing directories so we need to
  # specify absolute paths to files. Here we are getting the
  # root path that is used to build other paths.
  #**************************************************************
  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print log_FILE $sftp->lastErrorText() . "\n";
    return "0|Could not get absolute path\n";
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  my $dirListing = $sftp->ReadDir($handle);
  if ( $dirListing eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }
  else
  {
    my $n = $dirListing->get_NumFilesAndDirs();
    if ( $n == 0 )
    {
      print "No entries found in this directory." . "\r\n";
    }
    else
    {
      print "---> numb of files: $n\n";
      for ( my $i = 0 ; $i <= $n - 1 ; $i++ )
      {

        # fileObj is a CkSFtpFile
        my $fileObj = $dirListing->GetFileObject($i);

#         print $fileObj->filename() . "\r\n";
        my $FileName = $fileObj->filename();

        my $FileSize = $fileObj->get_Size32();
        if ($FileName =~ /^\./ || $FileSize == 0)
          {
            print "filename starts with a . or is empty\n";
          }
        else
          {
            my $sysTime  = new chilkat::SYSTEMTIME();
            $handle = $sftp->openFile( "$absPath/$DirToList$FileName", "readOnly", "openExisting" );
            if ( $handle eq "" )
              {
                print $sftp->lastErrorText();
                return "0|Could not open file on remote server\n";
               }
            print "Filename: $FileName\n";
            my $Date = $sftp->GetFileLastModified($handle, "", "1", $sysTime);
            #print "Last Modified: " . $sysTime->{wMonth} . "/" . $sysTime->{wDay} . "/" . $sysTime->{wYear} . "\n";
            print "Last Modified: " . $sysTime->{wMonth} . $sysTime->{wDay} . $sysTime->{wYear} . "\n";
            my $FileDate = $sysTime->{wMonth} . $sysTime->{wDay} . $sysTime->{wYear};

            # print $fileObj->fileType() . "\r\n";
            print "Size in bytes: " . $FileSize . "\r\n";
            # push( my @FileArrayRef, $fileObj->filename() );
            push(@$FileArrayRef, $fileObj->filename() ."|". $FileSize ."|".  $FileDate);
          }

        print "----" . "\r\n";
      } # for ( my $i = 0....
    }
    print $sftp->lastErrorText() . "\n";
      return 1;

    # return $dirListing;
  }



}    ##sub GetSftpFilelist

#*********************************************************************
#** SUBROUTINE THAT GETS A FILE VIA SFTP.
#*********************************************************************
sub GetSFtpFile
{

  # &GetFtpFile("ftp.usat.com", "elias2", "20elias205", "\incoming", $FileSpec, $OutputFileSpec);

  my $system         = $_[0];
  my $login          = $_[1];
  my $password       = $_[2];
  my $todir          = $_[3];
  my $FileSpec       = $_[4];
  my $OutputFileSpec = $_[5];

  print "**** $FileSpec\n";

  my $port = 22;

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;
  $sftp = new chilkat::CkSFtp();

  #**************************************************************
  # Standard code that connects and set up the ssh/sftp session
  #**************************************************************
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "connected to $system\n";
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.  This example uses password authenication.
  $success = $sftp->AuthenticatePw( "$login", "$password" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system\n";
    $ErrorMsg = &now . " Could not log on to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "logged on to $system\n";
  }

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "Initialized SFTP subsystem\n";
  }
  #**************************************************************
  # End of standard code that connects and set up the ssh/sftp session
  #**************************************************************

  #**************************************************************
  # sft does not support changing directories so we need to
  # specify absolute paths to files. Here we are getting the
  # root path that is used to build other paths.
  #**************************************************************
  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print log_FILE $sftp->lastErrorText() . "\n";
    return "0|Could not get absolute path\n";
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  # get the file here....
  #  Open a file on the server:

  print "--> $absPath/$FileSpec\n";
  $handle = $sftp->openFile( "$absPath/$FileSpec", "readOnly", "openExisting" );
  if ( $handle eq "" )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    return "0|Could not open file on remote server\n";;
  }

  #  Download the file:
  print "--> $OutputFileSpec\n";
  $success = $sftp->DownloadFile( $handle, $OutputFileSpec );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    return "0|Unable to download $absPath/$FileSpec from $system\n";
  }

  #  Close the file.
  $success = $sftp->CloseHandle($handle);
  if ( $success != 1 )
  {
    print $sftp->lastErrorText() . "\n";
    return 0;
  }

}
# END GetSFtpFile

#*********************************************************************
#** Sends a file via sFTP with an RSA Key
#*********************************************************************
#
sub send_sFTPwKey
{

  my $system         = $_[0];
  my $login          = $_[1];
  my $password       = $_[2];
  my $todir          = $_[3];
  my $filename       = $_[4];
  my $rsa_key        = $_[5];
  my $port           = $_[6];

  my $handle;
  my $success;
  my $ErrorMsg;

  my $sftp = new chilkat::CkSFtp();
  my $ssh  = new chilkat::CkSsh();

  #**************************************************************
  # Standard code that connects and set up the ssh/sftp session
  #**************************************************************
  #  Important: It is helpful to send the contents of the
  #  sftp.LastErrorText property when requesting support.

  #  Any string automatically begins a fully-functional 30-day trial.
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(10000);
  $sftp->put_IdleTimeoutMs(15000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " connected to $system\n";
  }

  my $key = new chilkat::CkSshKey();

  #  Load a private key from a PEM file:
  #  (Private keys may be loaded from OpenSSH and Putty formats.
  #  Both encrypted and unencrypted private key file formats
  #  are supported.  This example loads an unencrypted private
  #  key in OpenSSH format.

  # my $privKey = $key->loadText("c:/keys/id_rsa");
  my $privKey = $key->loadText($rsa_key);

  # if ($privKey eq null ) {
  if ( $privKey eq '' )
  {
    print "2:Error\n";
    print $key->lastErrorText() . "\n";
    print LOG_FILE &now, $key->lastErrorText() . "\n";
    return "0|Could not load rsa key: $rsa_key";
    # exit;
  }

  # $success = $key->FromPuttyPrivateKey($privKey);
  $success = $key->FromOpenSshPrivateKey($privKey);
  if ( $success != 1 )
  {
    print "3:Error\n";
    print $key->lastErrorText() . "\n";
    print LOG_FILE &now, $key->lastErrorText() . "\n";
    return "0|Could not load private key";
    # exit;
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.
  $success = $sftp->AuthenticatePk( $login, $key );

  # $success = $ssh->AuthenticatePk($login,$key);

  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system with $key\n";
    $ErrorMsg = &now . " Could not log on to $system with $key\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " logged on to $system\n";
  }

  print $sftp->lastErrorText() . "\n";
  print "Public-Key Authentication Successful!" . "\n";

  ##########################################################
  #
  # start of file upload code
  #
  ##########################################################

  # The filename is passed with the full path.
  #
  my ($FileUtilHandle) = File::Util->new();
  my $short_filename = $FileUtilHandle->strip_path($filename);

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Initialized SFTP subsystem\n";
  }

  #  To find the full path of our user account's home directory,
  #  call RealPath like this:

  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    return "0|Could not determine absolute path";
    #exit;
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  #  Open a file for writing on the SSH server.
  #  If the file already exists, it is overwritten.
  #  (Specify "createNew" instead of "openOrCreate" to
  #  prevent overwriting existing files.)

  print "remote file: $absPath" . "/" . "$todir$short_filename\n";

  # print "remote file: $absPath" . "/" . "$todir$filename\n";
  # $handle = $sftp->openFile( "$absPath" . "/" . "$todir$short_filename", "writeOnly", "openOrCreate" );
  $handle = $sftp->openFile( "$absPath$todir$short_filename", "writeOnly", "openOrCreate" );

  # if ( $handle eq null )
  if ( $handle eq "" )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not open file in remote system\n";
    $ErrorMsg = &now . " Could not open file in remote system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " Opened remote file: $absPath$todir$short_filename\n";
  }

  #  Upload from the local file to the SSH server.
  print LOG_FILE &now, "local file to upload: $filename\n";
  $success = $sftp->UploadFile( $handle, "$filename" );

  # $success = $sftp->UploadFile( $handle, "$InputDir$filename" );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not upload file to remote system\n";
    $ErrorMsg = &now . " Could not upload file to remote system\n";
    return "0|$ErrorMsg";
  }

  #  Close the file.
  $success = $sftp->CloseHandle($handle);
  if ( $success != 1 )
  {
    print "closehandle: " . $sftp->lastErrorText() . "\n";
    return "0|Could not close sftp filehandle";
    # exit;
  }

  print "Success." . "\n";

}    # END send_sFTPwKEY

#*********************************************************************
#** SUBROUTINE THAT GETS A FILE VIA SFTP.
#*********************************************************************
sub Get_sFTPwKey
{
  # &Get_sFTPwKey("ftp.usat.com", $login, "", "\incoming", "test_download.txt", "test_download.txt", "7966");

  my $system         = $_[0];
  my $login          = $_[1];
  my $password       = $_[2];
  my $todir          = $_[3];
  my $FileSpec       = $_[4];
  my $OutputFileSpec = $_[5];
  my $port           = $_[6];

  print "**** $FileSpec\n";

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;
  my $sftp = new chilkat::CkSFtp();
  my $ssh  = new chilkat::CkSsh();

  #**************************************************************
  # Standard code that connects and set up the ssh/sftp session
  #**************************************************************
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "connected to $system\n";
  }

  my $key = new chilkat::CkSshKey();

  #  Load a private key from a PEM file:
  #  (Private keys may be loaded from OpenSSH and Putty formats.
  #  Both encrypted and unencrypted private key file formats
  #  are supported.  This example loads an unencrypted private
  #  key in OpenSSH format.

  my $privKey = $key->loadText("id_rsa");

  # if ($privKey eq null ) {
  if ( $privKey eq '' )
  {
    print "2:Error\n";
    print $key->lastErrorText() . "\n";
    exit;
  }

  # $success = $key->FromPuttyPrivateKey($privKey);
  $success = $key->FromOpenSshPrivateKey($privKey);
  if ( $success != 1 )
  {
    print "3:Error\n";
    print $key->lastErrorText() . "\n";
    exit;
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.
  $success = $sftp->AuthenticatePk( $login, $key );

  # $success = $ssh->AuthenticatePk($login,$key);

  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system with $key\n";
    $ErrorMsg = &now . " Could not log on to $system with $key\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " logged on to $system\n";
  }

  print $sftp->lastErrorText() . "\n";
  print "Public-Key Authentication Successful!" . "\n";

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "Initialized SFTP subsystem\n";
  }

  #**************************************************************
  # End of standard code that connects and set up the ssh/sftp session
  #**************************************************************

  #**************************************************************
  # sft does not support changing directories so we need to
  # specify absolute paths to files. Here we are getting the
  # root path that is used to build other paths.
  #**************************************************************
  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print log_FILE $sftp->lastErrorText() . "\n";
    return "0|Could not get absolute path\n";
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  # get the file here....
  #  Open a file on the server:

  print "--> $absPath/$FileSpec\n";
  # $handle = $sftp->openFile( "$absPath/$FileSpec", "readOnly", "openExisting" );
  $handle = $sftp->openFile( "$todir$FileSpec", "readOnly", "openExisting" );
  if ( $handle eq "" )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    return "0|Could not open file on remote server\n";
  }

  #  Download the file:
  print "--> $OutputFileSpec\n";
  $success = $sftp->DownloadFile( $handle, $OutputFileSpec );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    return "0|Unable to download $absPath/$FileSpec from $system\n";
  }

  #  Close the file.
  $success = $sftp->CloseHandle($handle);
  if ( $success != 1 )
  {
    print $sftp->lastErrorText() . "\n";
    return 0;
  }

}

# END Get_sFTPwKey

#*********************************************************************
#** SUBROUTINE THAT GETS A DIRECTORY VIA SFTP.
#*********************************************************************
sub GetSftpFilelistwKey
{

  my $system = $_[0];
  my $login = $_[1];
  my $password = $_[2];
  my $DirToList = $_[3];
  my $FileSpec = $_[4];
  my $FileArrayRef = $_[5];
  my $rsa_key        = $_[6];
  my $port           = $_[7];

  print "---> Files to get: $FileSpec\n";

  my $port = 22;

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;
  my $sftp = new chilkat::CkSFtp();
  my $ssh  = new chilkat::CkSsh();

  #**************************************************************
  # Standard code that connects and set up the ssh/sftp session
  #**************************************************************
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "connected to $system\n";
  }

  my $key = new chilkat::CkSshKey();

  #  Load a private key from a PEM file:
  #  (Private keys may be loaded from OpenSSH and Putty formats.
  #  Both encrypted and unencrypted private key file formats
  #  are supported.  This example loads an unencrypted private
  #  key in OpenSSH format.

  my $privKey = $key->loadText($rsa_key);
  # my $privKey = $key->loadText("c:/keys/id_rsa");

  # if ($privKey eq null ) {
  if ( $privKey eq '' )
  {
    print "2:Error\n";
    print $key->lastErrorText() . "\n";
    exit;
  }

  # $success = $key->FromPuttyPrivateKey($privKey);
  $success = $key->FromOpenSshPrivateKey($privKey);
  if ( $success != 1 )
  {
    print "3:Error\n";
    print $key->lastErrorText() . "\n";
    exit;
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.  This example uses password authenication.
  # $success = $sftp->AuthenticatePw( "$login", "$password" );

  $success = $sftp->AuthenticatePk( $login, $key );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system\n";
    $ErrorMsg = &now . " Could not log on to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "logged on to $system\n";
  }

  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "Initialized SFTP subsystem\n";
  }
  #**************************************************************
  # End Standard code that connects and set up the ssh/sftp session
  #**************************************************************

  $handle = $sftp->openDir($DirToList);
  if ( $handle eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }

####**************************************************************
#### New get dir list routine. returns file size and date as well as name
####**************************************************************

  #**************************************************************
  # sft does not support changing directories so we need to
  # specify absolute paths to files. Here we are getting the
  # root path that is used to build other paths.
  #**************************************************************
  my $absPath = $sftp->realPath( ".", "" );
  if ( $absPath eq "" )
  {
    print log_FILE $sftp->lastErrorText() . "\n";
    return "0|Could not get absolute path\n";
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  my $dirListing = $sftp->ReadDir($handle);
  if ( $dirListing eq "" )
  {
    print $sftp->lastErrorText() . "\n";
    exit;
  }
  else
  {
    my $n = $dirListing->get_NumFilesAndDirs();
    if ( $n == 0 )
    {
      print "No entries found in this directory." . "\r\n";
    }
    else
    {
      print "---> numb of files: $n\n";
      for ( my $i = 0 ; $i <= $n - 1 ; $i++ )
      {

        # fileObj is a CkSFtpFile
        my $fileObj = $dirListing->GetFileObject($i);

        print "filetype: " . $fileObj->fileType() . "\r\n";
        my $FileName = $fileObj->filename();

        if ($fileObj->fileType() =~ /directory/i)
          {
            print $fileObj->filename() . " is a directory\n";
          }
        else
          {
            my $FileSize = $fileObj->get_Size32();
#            if ($FileName =~ /^\./ || $FileSize == 0)
#              {
#                print "filename starts with a . or is empty\n";
#              }
#            else
#              {
                my $sysTime  = new chilkat::SYSTEMTIME();
#                $handle = $sftp->openFile( "$absPath/$DirToList$FileName", "readOnly", "openExisting" );
#                if ( $handle eq "" )
#                  {
#                    print $sftp->lastErrorText();
#                    return "0|Could not open file on remote server\n";
#                   }
                print "Filename: $FileName\n";
                my $Date = $sftp->GetFileLastModified($handle, "", "1", $sysTime);
                #print "Last Modified: " . $sysTime->{wMonth} . "/" . $sysTime->{wDay} . "/" . $sysTime->{wYear} . "\n";
                print "Last Modified: " . $sysTime->{wMonth} . $sysTime->{wDay} . $sysTime->{wYear} . "\n";
                my $FileDate = $sysTime->{wMonth} . $sysTime->{wDay} . $sysTime->{wYear};

                # print $fileObj->fileType() . "\r\n";
                print "Size in bytes: " . $FileSize . "\r\n";
                # push( my @FileArrayRef, $fileObj->filename() );
                push(@$FileArrayRef, $fileObj->filename() ."|". $FileSize ."|".  $FileDate);
#              }
            }
        print "----" . "\r\n";
      } # for ( my $i = 0....
    }
    # print $sftp->lastErrorText() . "\n";
      return 1;

    # return $dirListing;
  }


}    #sub GetFtpFilelist

#*********************************************************************
#** SUBROUTINE THAT GETS A FILE VIA SFTP.
#*********************************************************************
sub GetSFtpFilewKey
{

  # &GetFtpFile("ftp.usat.com", "elias2", "20elias205", "\incoming", $FileSpec, $OutputFileSpec);

  my $system         = $_[0];
  my $login          = $_[1];
  my $password       = $_[2];
  my $todir          = $_[3];
  my $FileSpec       = $_[4];
  my $OutputFileSpec = $_[5];
  my $rsa_key        = $_[6];
  my $port           = $_[7];

  my $sftp;
  my $handle;
  my $success;
  my $ErrorMsg;
  $sftp = new chilkat::CkSFtp();

  # &SFTP_ConnectWithKey

  #**************************************************************
  # Standard code that connects and set up the ssh/sftp session
  #**************************************************************
  $success = $sftp->UnlockComponent("USATODSSH_8XqbH0fB6Pno");
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not activate SFTP module\n";
    $ErrorMsg = &now . " Could not activate SFTP module\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "activated chilkat module\n";
  }

  #  Set some timeouts, in milliseconds:
  $sftp->put_ConnectTimeoutMs(5000);
  $sftp->put_IdleTimeoutMs(10000);

  #  Connect to the SSH server.
  #  The standard SSH port = 22
  #  The hostname may be a hostname or IP address.

  $success = $sftp->Connect( $system, $port );
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not connect to $system\n";
    $ErrorMsg = &now . " Could not connect to $system\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "connected to $system\n";
  }

  my $key = new chilkat::CkSshKey();

  #  Load a private key from a PEM file:
  #  (Private keys may be loaded from OpenSSH and Putty formats.
  #  Both encrypted and unencrypted private key file formats
  #  are supported.  This example loads an unencrypted private
  #  key in OpenSSH format.

  # my $privKey = $key->loadText("c:/keys/id_rsa");
  my $privKey = $key->loadText($rsa_key);

  # if ($privKey eq null ) {
  if ( $privKey eq '' )
  {
    print "2:Error\n";
    print $key->lastErrorText() . "\n";
    exit;
  }

  # $success = $key->FromPuttyPrivateKey($privKey);
  $success = $key->FromOpenSshPrivateKey($privKey);
  if ( $success != 1 )
  {
    print "3:Error\n";
    print $key->lastErrorText() . "\n";
    exit;
  }

  #  Authenticate with the SSH server.  Chilkat SFTP supports
  #  both password-based authenication as well as public-key
  #  authentication.
  $success = $sftp->AuthenticatePk( $login, $key );

  # $success = $ssh->AuthenticatePk($login,$key);

  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not log on to $system with $key\n";
    $ErrorMsg = &now . " Could not log on to $system with $key\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, " logged on to $system\n";
  }

  # print $sftp->lastErrorText() . "\n";
  print "Public-Key Authentication Successful!" . "\n";


  #  After authenticating, the SFTP subsystem must be initialized:
  $success = $sftp->InitializeSftp();
  if ( $success != 1 )
  {
    print LOG_FILE $sftp->lastErrorText() . "\n";
    print $sftp->lastErrorText() . "\n";
    print LOG_FILE &now, " !!! Could not initialize sftp subsystem\n";
    $ErrorMsg = &now . " Could not initialize sftp subsystem\n";
    return "0|$ErrorMsg";
  }
  else
  {
    print LOG_FILE &now, "Initialized SFTP subsystem\n";
  }
  #**************************************************************
  # End of standard code that connects and set up the ssh/sftp session
  #**************************************************************

  #**************************************************************
  # sftp does not support changing directories so we need to
  # specify absolute paths to files. Here we are getting the
  # root path that is used to build other paths.
  #**************************************************************
  my $absPath = $sftp->realPath( ".", "" );

  if ( $absPath eq "" )
  {
    print log_FILE $sftp->lastErrorText() . "\n";
    return "0|Could not get absolute path\n";
  }
  else
  {
    print LOG_FILE &now, " Absolute path: " . $absPath . "\r\n";
  }

  $absPath = "";

  # get the file here....
  #  Open a file on the server:

  print LOG_FILE "--> $absPath/$FileSpec\n";
  print "--> $absPath/$FileSpec\n";
  print "Dwownloading file\n";

  $success = $sftp->DownloadFileByName( "$absPath/$FileSpec", $OutputFileSpec );
  if ( $success != 1 )
  {
    print $sftp->lastErrorText() . "\n";
    return 0;
  }

  return "1|Succcess";

}
# END GetSFtpFile