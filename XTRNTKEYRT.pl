#################################################################################
#
# XTRNTKEYRT.PL
#
# Created 12/2000 by Scott Seller
#
# updated 8/6/2002 Scott Seller Added code to check for a zero length file.
#                               Added send_alert subroutine.
# updated 05/22/2007 Houman Ghavami - Added an error email counter.  Only send 5 email errors
#								Successful exectution requires notify.txt be in the same directory as the script
#
# A seperate process (FILE_SCAN tool) monitors the directory and runs this script.
# 
# Update - SWONG  Add subroutine to eliminate " ' " that causes error on loads
#################################################################################
use Win32::ODBC;
use Net::SMTP;
use File::stat;

#Handle special characters - swong
my %chgspecial = ("\'"," ");
my @special = %chgspecial;
my $symbol;
my $symboltxt;
my $newbasename;

my($db) = new Win32::ODBC("dsn=esub; uid=usatSPODBC; pwd=subscribe");

$db->SetConnectOption('SQL_AUTOCOMMIT', 'SQL_AUOTCOMMIT_OFF');

&CreateTimeStamp();

$ACTIVE = "N";
$counter = 0;
$err_counter = 0;
$ErrorCounter = 0;
$file_size = 0;
$LogFileName = "XTRNKEYRT_" . $year . $month . $day . ".log";
$timestamp = localtime;

$mailServer = "smtp.v4.gmti.gbahn.net";

open (LOG_FILE, ">>L:\\USAT\\processed\\logs\\$LogFileName");

### Create email/page list

if (open(NL, "L:\\USAT\\scripts\\notify_list.txt"))
	{
		@notify = (<NL>);
		close NL;
	}
else
	{
		print "Error opening notify_list.txt\n";
		print LOG_FILE "$timestamp -- Error opening notify_list.txt\n";
		die;
	}	

### Check to make sure we didn't get an empty file

$file_size = stat("L:\\USAT\\FTP\\incoming\\XTRNTKEYRT")->size;
if ($file_size == 0)
	{
		print LOG_FILE "$timestamp -- XTRNTKEYRT is empty.\n";
		&send_alert("$timestamp -- XTRNTKEYRT is empty.\n", "Empty XTRNTKEYRT From 400");
		rename ("L:\\USAT\\FTP\\incoming\\XTRNTKEYRT", "L:\\USAT\\processed\\XTRNTKEYRT");
		die;
	}

open (EXTRACT_FILE, "L:\\USAT\\FTP\\incoming\\XTRNTKEYRT");
print LOG_FILE "$timestamp -- Started processing XTRNTKEYRT\n";
print "$timestamp -- Started processing XTRNTKEYRT\n";

while (<EXTRACT_FILE>)
	{
		print ".";
		$PubCode = substr($_,0,2);
		$FreqDel = substr($_,2,2);
		$SrcOrderCode = substr($_,4,1);
		$PromoCode = substr($_,5,2);
		$ContestCode= substr($_,7,2);
		$RateCode = substr($_,9,2);
		$RateCodeType = substr($_,11,1);
		$RetailRate = substr($_,12,7);
		$SubsLength = substr($_,19,3);
		$SubsAmount = substr($_,22,7);
		$OfferDesc= substr($_,29,50);
		$OperatorNum = substr($_,79,3);
		$BegDateOffer = substr($_,82,8);
		$EndDateOffer= substr($_,90,8);
		$BillMeAllowed= substr($_,98,1);
		$ForceEZPay= substr($_,99,1);
		$ClubNumberRequired= substr($_,100,1);

		$counter++;

		convert_from_specialchar();
		
		if ($db->Sql("INSERT INTO XTRNTKEYRT 
					(PubCode, FreqDel, SrcOrderCode, PromoCode, ContestCode, RateCode, RateCodeType, RetailRate, 
					SubsLength, SubsAmount, OfferDesc, OperatorNum, BegDateOffer, EndDateOffer, ACTIVE, BillMeAllowed, ForceEZPay, ClubNumberRequired)
					VALUES 
					('$PubCode', '$FreqDel', '$SrcOrderCode', '$PromoCode', '$ContestCode', '$RateCode', '$RateCodeType', 
					'$RetailRate', '$SubsLength', '$SubsAmount', '$OfferDesc', '$OperatorNum', '$BegDateOffer', 
					'$EndDateOffer','$ACTIVE','$BillMeAllowed', '$ForceEZPay', '$ClubNumberRequired')"))
			{
			$err_counter++;
			#$db->Transact('SQL_ROLLBACK');
			my($err) = $db->Error;
			warn "Sql() ERROR\n";
			warn "\t\$stmt: INSERT INTO XTRNTKEYRT...\n";
			warn "\t\$err: $err\n";
			# send an email
			$eMsg = "pubcode='$PubCode', freqdel='$FreqDel', srcordercode='$SrcOrderCode', promoCode='$PromoCode'\n, ContestCode='$ContestCode', RateCode='$RateCode', RateCodeType='$RateCodeType',\n 
					retailRate='$RetailRate', subslength='$SubsLength', subsamount='$SubsAmount', offerDesc='$OfferDesc',\n OperatorNum='$OperatorNum', BegDateOffer='$BegDateOffer', 
					EndDateOffer='$EndDateOffer', Active='$ACTIVE', BillMeAllowed='$BillMeAllowed', ForceEZPay='$ForceEZPay', ClubNumberRequired='$ClubNumberRequired'";
                        # Issue up to 5 error emails
                        if ($ErrorCounter < 5) {
				&send_alert("$timestamp -- \n\nError Code: $err\n\nApplication Terminated. Manual Intervention required.\n\nDATA: $eMsg", "Database error Inserting into XTRNTKEYRT");
                                $ErrorCounter++;
                        }			



			exit;
		}	

} # while (<EXTRACT_FILE>)

# $db->Transact('SQL_COMMIT');

# Remove old records

$timestamp = localtime;
print LOG_FILE "$timestamp -- Deleting old records\n";
print "$timestamp -- Deleting old records\n";
$db->Sql("DELETE FROM XTRNTKEYRT WHERE ACTIVE = 'Y'"); 

# Update newly inserted records.
$timestamp = localtime;
print LOG_FILE "$timestamp -- setting active flag to Y\n";
print "$timestamp -- setting active flag to Y\n";
$db->Sql("UPDATE XTRNTKEYRT SET ACTIVE = 'Y'"); 

$db->Close();
$end_time = localtime;
print LOG_FILE "$counter records inserted\n";
print LOG_FILE "$err_counter records not processed\n";
print LOG_FILE "$end_time -- Finished processing XTRNTKEYRT\n";
print LOG_FILE "-----------------------------\n";
close (EXTRACT_FILE);
close (LOG_FILE);

$timestamp = join('_', split(/:/, localtime));
# $timestamp =~ join('_', split(/ /, $timestamp));
$new_name = "XTRNTKEYRT_" . $timestamp;
rename ("L:\\USAT\\FTP\\incoming\\XTRNTKEYRT", "L:\\USAT\\processed\\$new_name");

sub send_alert
	{
		foreach $SendTo (@notify)
			{
				$msg = $_[0];
				$subject = $_[1];

				$smtp = Net::SMTP->new($mailServer); 	# connect to an SMTP server
				$smtp->mail( 'XTRNTKEYRT_PERL@usatoday.com' );     		# use the sender's address here
				$smtp->to($SendTo);        				# recipient's address

				$smtp->data();                      			# Start the mail

				# Send the header.
				$smtp->datasend("To: $SendTo\n");
				$smtp->datasend("From: moc-wn1048\n");
				$smtp->datasend("Subject: $subject\n");
				$smtp->datasend("\n");

				# Send the body.
				$smtp->datasend($msg);
				$smtp->dataend();                   # Finish sending the mail
				$smtp->quit;                        # Close the SMTP
				

				print LOG_FILE $timestamp . " -- email/page sent to: $SendTo\n";
			}	
	}


#Search array for special characters and text value -  swong
sub convert_from_specialchar {
	
	$newbasename = $OfferDesc;
	while (($symbol, $symboltxt) = each(%chgspecial))
	{	  
	  if ($newbasename =~ s/$symbol/$symboltxt/g){
		print LOG_FILE "** $OfferDesc changed to - $newbasename *** \n";

		$OfferDesc = $newbasename;		
 
 	  } else {
		#skipped
	  } 
	}

}

sub CreateTimeStamp
	{
		# Get information to produce the name of the files
		($year)=((localtime)[5]) + 1900;
		($month) = ((localtime)[4]) + 1;
		if (length($month) == 1){$month = "0" . $month;} # pad single digit month
		($day) = ((localtime)[3]);
		if (length($day) == 1){$day = "0" . $day;} # pad single digit day
		($hour) = ((localtime)[2]);
		if (length($hour) == 1){$hour = "0" . $hour;} # pad single digit hour
		($min) = ((localtime)[1]);
		if (length($min) == 1){$min = "0" . $min;} # pad single digit minute
		($sec) = ((localtime)[0]);
		if (length($sec) == 1){$sec = "0" . $sec;} # pad single digit second
		return $year, $month, $day, $hour, $min, $sec;
	}